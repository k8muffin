/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "common.h"


////////////////////////////////////////////////////////////////////////////////
#ifdef NO_SEARCH_DBGLOG
# define sxdlogf(...)  ((void)0)
#else
# define sxdlogf  dlogf
#endif


////////////////////////////////////////////////////////////////////////////////
static int opt_files_only_in_files = 1;


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  const char *name;
  fl_fileinfo_t *fi;
  UT_hash_handle hh;
} filename_t;


static filename_t *sname_hash = NULL;
static filename_t *sname_data = NULL;


typedef struct {
  const char *value;
  fl_tagvalue_t *tv;
  UT_hash_handle hh;
} tagvalue_t;

// all tags
static tagvalue_t *tval_hash = NULL;
static tagvalue_t *tval_data = NULL;

static tagvalue_t *tspec_hash[TFL_MAX] = {NULL};
static tagvalue_t *tspec_data[TFL_MAX] = {NULL};


static fl_tagvalue_t *get_tag_by_name (const char *str, tagvalue_t **hash) {
  if (str != NULL && str[0]) {
    tagvalue_t *tv;
    HASH_FIND_STR(*hash, str, tv);
    return (tv != NULL ? tv->tv : NULL);
  }
  return NULL;
}


#define XCLR(var)  do { \
  HASH_CLEAR(hh, var##_hash); \
  if (var##_data != NULL) free(var##_data); \
  var##_data = NULL; \
  var##_hash = NULL; \
} while (0)

static void clear_hashes (void) {
  XCLR(sname);
  XCLR(tval);
  for (int f = 0; f < TFL_MAX; ++f) {
    HASH_CLEAR(hh, tspec_hash[f]);
    if (tspec_data[f] != NULL) free(tspec_data[f]);
    tspec_hash[f] = NULL;
    tspec_data[f] = NULL;
  }
}

#undef XCLR


// allocate full chunks, it's faster and better than malloc()ing each item separately
static void build_hashes (void) {
  sname_data = malloc(sizeof(sname_data[0])*file_count);
  for (uint32_t f = 0; f < file_count; ++f) {
    fl_fileinfo_t *fi = get_fi(f);
    filename_t *fn = &sname_data[f];
    fn->name = get_str(fi->shortname);
    fn->fi = fi;
    HASH_ADD_KEYPTR(hh, sname_hash, fn->name, strlen(fn->name), fn);
  }
  printf("%d snames in hash\n", HASH_COUNT(sname_hash));
  //
  tval_data = malloc(sizeof(tval_data[0])*tag_count);
  for (uint32_t f = 0; f < tag_count; ++f) {
    fl_tagvalue_t *tv = get_tv(f);
    tagvalue_t *t = &tval_data[f];
    //if (tv->value >= string_bytes) abort();
    t->value = get_str(tv->value);
    t->tv = tv;
    HASH_ADD_KEYPTR(hh, tval_hash, t->value, strlen(t->value), t);
  }
  printf("%d tags in hash\n", HASH_COUNT(tval_hash));
  //
  int spcnt[TFL_MAX];
  for (int f = 0; f < TFL_MAX; ++f) {
    spcnt[f] = 0;
    tspec_data[f] = malloc(sizeof(tspec_data[f][0])*tag_count); //FIXME: too much
  }
  for (uint32_t f = 0; f < file_count; ++f) {
    //const fl_fileinfo_t *fi = get_fi_idx(f);
    for (int tn = 0; tn < TFL_MAX; ++tn) {
      tagvalue_t *tv;
      fl_tagvalue_t *ftv = get_tv(get_fi(f)->tags[tn]);
      const char *str = get_str(ftv->value);
      HASH_FIND_STR(tspec_hash[tn], str, tv);
      if (tv == NULL) {
        tv = &tspec_data[tn][spcnt[tn]++];
        tv->value = str;
        tv->tv = ftv;
        HASH_ADD_KEYPTR(hh, tspec_hash[tn], tv->value, strlen(tv->value), tv);
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
#define ARRAYLEN(arr)  (sizeof((arr))/sizeof((arr)[0]))


typedef enum {
  SPT_ANY,
  SPT_ORIG,
  SPT_TRANS
} special_dir_type;


static const special_dir_type tagtype[TFL_MAX] = {
  SPT_ANY, // TFL_YEAR
  SPT_ORIG, // TFL_ARTIST_O
  SPT_TRANS, // TFL_ARTIST_T
  SPT_ORIG, // TFL_ALBUM_O
  SPT_TRANS, // TFL_ALBUM_T
  SPT_ORIG, // TFL_TITLE_O
  SPT_TRANS, // TFL_TITLE_T
  SPT_ORIG, // TFL_GENRE_O
  SPT_TRANS, // TFL_GENRE_T
};


typedef struct {
  const char *name; // with colon
  special_dir_type type;
  int tflidx;
} special_dir_info_t;


static const special_dir_info_t spec_dirs[] = {
  {.name=":oartist", .type=SPT_ORIG, .tflidx=TFL_ARTIST_O},
  {.name=":oalbum", .type=SPT_ORIG, .tflidx=TFL_ALBUM_O},
  {.name=":otitle", .type=SPT_ORIG, .tflidx=TFL_TITLE_O},
  //{.name=":ogenre", .type=SPT_ORIG, .tflidx=TFL_GENRE_O},
  {.name=":year", .type=SPT_ANY, .tflidx=TFL_YEAR},
  {.name=":artist", .type=SPT_TRANS, .tflidx=TFL_ARTIST_T},
  {.name=":album", .type=SPT_TRANS, .tflidx=TFL_ALBUM_T},
  {.name=":title", .type=SPT_TRANS, .tflidx=TFL_TITLE_T},
  //{.name=":genre", .type=SPT_TRANS, .tflidx=TFL_GENRE_T},
};


////////////////////////////////////////////////////////////////////////////////
static inline const special_dir_info_t *get_special_by_name (const char *name) {
  if (name != NULL && name[0] == ':') {
    for (size_t f = 0; f < ARRAYLEN(spec_dirs); ++f) {
      if (strcmp(spec_dirs[f].name, name) == 0) return &spec_dirs[f];
    }
  }
  return NULL;
}


static inline fl_tagvalue_t *get_tag_with_special (const char *name, const special_dir_info_t *sp) {
  return (sp != NULL ? get_tag_by_name(name, &tspec_hash[sp->tflidx]) : get_tag_by_name(name, &tval_hash));
}


////////////////////////////////////////////////////////////////////////////////
#define FIDX_NONE  (0)

typedef struct {
  const char *name; // short names
  uint32_t fidx; // file index+1 (if any) or FIDX_NONE
} diritem_t;

typedef struct {
  diritem_t *items;
  int fcount;
} dirinfo_t;


typedef struct {
  uint32_t idx;
  UT_hash_handle hh;
} u32value_t;


static inline int zdifsign (uint32_t n0, uint32_t n1) {
  return (n0 < n1 ? -1 : (n0 > n1 ? 1 : 0));
}


static int has_dir (const char *path, const char *dir) {
  int dlen = strlen(dir);
  for (;;) {
    const char *p = strstr(path, dir);
    if (p == NULL) return 0;
    if ((p == path || p[-1] == '/') && (p[dlen] == '/' || p[dlen] == 0)) return 1;
    path = p+dlen;
  }
}


static int count_dirs (const char *path) {
  int res = 0;
  while (*path) if (*path++ == '/') ++res;
  return res;
}


// /tag/tag/tag/
// /:artist/name/:album/name/
static dirinfo_t *list_dir_simple (const char *path) {
  u32value_t *files = NULL; // NULL: all files in list
  u32value_t *fhash = NULL; // to ease searching by id
  uint32_t pos = 0;
  int onlyfiles = 0;
  special_dir_type sptype = SPT_ANY;
  const special_dir_info_t *sdi = NULL;
  char *condstr = alloca(strlen(path)+1);
  u32value_t *tl_hash = NULL, *hx, *thx;
  dirinfo_t *di;
  int firsttime = 1;
  //
  //TODO: accelerate this with hash
  void add_from_tv (const fl_tagvalue_t *tv, const special_dir_info_t *sp, special_dir_type tp) {
    // just add all files
    uint32_t cnt = 0;
    //sxdlogf("add_from_tv: [%s]; sp=%p; tp=%d\n", get_str(tv->value), sp, tp);
    // let it be MANY
    for (int tn = 0; tn < TFL_MAX; ++tn) {
      //sxdlogf(" tn=%d; count=%u\n", tn, tv->tfl[tn].count);
      cnt += tv->tfl[tn].count;
    }
    files = malloc(sizeof(files[0])*cnt+1);
    cnt = 0;
    for (int tn = 0; tn < TFL_MAX; ++tn) {
      const fl_tfl_t *tfl;
      u32value_t *fl;
      if (sp != NULL && tn != sp->tflidx) continue;
      if (sp == NULL && tp != SPT_ANY && tagtype[tn] != SPT_ANY && tp != tagtype[tn]) continue;
      tfl = &tv->tfl[tn];
      //sxdlogf(" *:tn=%d; count=%u\n", tn, tfl->count);
      for (uint32_t f = 0; f < tfl->count; ++f) {
        uint32_t idx = tfl_data[tfl->idx+f];
        //sxdlogf("  %u: idx=%u\n", f, tfl_data[tfl->idx+f]);
        HASH_FIND_UINT32(fhash, &idx, fl);
        if (fl == NULL) {
          fl = &files[cnt++];
          fl->idx = idx;
          HASH_ADD_UINT32(fhash, idx, fl);
        }
      }
    }
  }
  //
  void remove_not_have_tv (const fl_tagvalue_t *tv, const special_dir_info_t *sp, special_dir_type tp) {
    // remove all files that have no such tag in given field
    u32value_t *cf, *cftmp;
    HASH_ITER(hh, fhash, cf, cftmp) {
      const fl_fileinfo_t *fi = get_fi(cf->idx);
      int dodrop = 1;
      for (int tn = 0; tn < TFL_MAX; ++tn) {
        if (sp != NULL && tn != sp->tflidx) continue;
        if (sp == NULL && tp != SPT_ANY && tagtype[tn] != SPT_ANY && tp != tagtype[tn]) continue;
        //sxdlogf("checking (%d)[%s]: %u : %u\n", tn, get_str(fi->shortname), fi->tags[tn], tv2idx(tv));
        if (fi->tags[tn] == tv2idx(tv)) { dodrop = 0; break; }
      }
      if (dodrop) HASH_DEL(fhash, cf);
    }
  }
  //
  char has_flags[ARRAYLEN(spec_dirs)];
  int spdcnt = 0;
  int no_spfiles = 0;
  int only_albums = 0;
  //
  if (strcmp(path, "/") == 0) {
    // root, list only artists
    for (uint32_t f = 0; f < file_count; ++f) {
      const fl_fileinfo_t *fi = get_fi(f);
      uint32_t idx = fi->artist_t;
      HASH_FIND_UINT32(tl_hash, &idx, hx);
      if (hx == NULL) {
        hx = calloc(1, sizeof(*hx));
        hx->idx = fi->artist_t;
        HASH_ADD_UINT32(tl_hash, idx, hx);
      }
    }
    di = calloc(1, sizeof(*di));
    di->fcount += HASH_COUNT(tl_hash);
    di->items = calloc(di->fcount+1, sizeof(di->items[0]));
    // tags
    HASH_ITER(hh, tl_hash, hx, thx) {
      di->items[pos++].name = get_str(get_tv(hx->idx)->value);
      HASH_DEL(tl_hash, hx);
      free(hx);
    }
    goto ret_sort_di;
  }
  //
  if (strstr(path, "/:") == NULL ) {
    int dc = count_dirs(path);
    if (dc < 3) {
      if (dc == 1) {
        // add all artist's albums
        const fl_tagvalue_t *tv_artist = get_tag_by_name(path+1, &tspec_hash[TFL_ARTIST_T]);
        for (uint32_t f = 0; f < file_count; ++f) {
          const fl_fileinfo_t *fi = get_fi(f);
          if (fi->artist_t == tv2idx(tv_artist)) {
            uint32_t idx = fi->yral_t;
            HASH_FIND_UINT32(tl_hash, &idx, hx);
            if (hx == NULL) {
              hx = calloc(1, sizeof(*hx));
              hx->idx = fi->yral_t;
              HASH_ADD_UINT32(tl_hash, idx, hx);
            }
          }
        }
        di = calloc(1, sizeof(*di));
        di->fcount += HASH_COUNT(tl_hash);
        di->items = calloc(di->fcount+1, sizeof(di->items[0]));
        // tags
        HASH_ITER(hh, tl_hash, hx, thx) {
          di->items[pos++].name = get_str(get_tv(hx->idx)->value);
          HASH_DEL(tl_hash, hx);
          free(hx);
        }
        goto ret_sort_di;
      } else if (dc == 2) {
        // add all album's songs
        strcpy(condstr, path+1);
        strchr(condstr, '/')[0] = 0;
        fl_tagvalue_t *tv_artist = get_tag_by_name(condstr, &tspec_hash[TFL_ARTIST_T]);
        fl_tagvalue_t *tv_album = get_tag_by_name(strchr(path+1, '/')+1, &tspec_hash[TFL_YRAL_T]);
        files = malloc(sizeof(files[0])*tv_album->tfl[TFL_YRAL_T].count+1);
        int cnt = 0;
        for (uint32_t f = 0; f < file_count; ++f) {
          fl_fileinfo_t *fi = get_fi(f);
          if (fi->artist_t == tv2idx(tv_artist) && fi->yral_t == tv2idx(tv_album)) {
            u32value_t *v = &files[cnt++];
            v->idx = f;
            HASH_ADD_UINT32(fhash, idx, v);
          }
        }
        di = calloc(1, sizeof(*di));
        di->fcount += HASH_COUNT(fhash);
        di->items = calloc(di->fcount+1, sizeof(di->items[0]));
        if (HASH_COUNT(fhash) > 0) {
          for (u32value_t *cf = fhash; cf != NULL; cf = cf->hh.next) {
            if (pos >= di->fcount) { dlogf("FATAL: internal error in searcher!\n"); abort(); }
            di->items[pos].fidx = cf->idx+1; //NOTE +1!
            di->items[pos++].name = get_str(get_fi(cf->idx)->shortname);
          }
        }
        HASH_CLEAR(hh, fhash);
        if (files != NULL) free(files);
        goto ret_sort_di;
      }
    }
  }
  //
  for (int f = 0; f < ARRAYLEN(spec_dirs); ++f) has_flags[f] = has_dir(path, spec_dirs[f].name);
  // we always has ':artist' (as root)
  has_flags[4] = 1;
  for (int f = 0; f < 3; ++f) if (has_flags[f+0] || has_flags[f+4]) has_flags[f+0] = has_flags[f+4] = 1;
  // don't list song titles if we has no ':title'
  only_albums = !has_flags[2];
  // if we has ':album', we don't need ':title'
  if (has_flags[1] || has_flags[4]) has_flags[2] = has_flags[6] = 1;
  // album, oalbum, year
  if (has_flags[1] || has_flags[3] || has_flags[5]) has_flags[1] = has_flags[3] = has_flags[5] = 1;
  //
  for (int f = 0; f < ARRAYLEN(spec_dirs); ++f) if (!has_flags[f]) ++spdcnt;
  //
  di = calloc(1, sizeof(*di));
  //
  if (strcmp(path, "/:ttags") == 0 || strcmp(path, "/:otags/:ttags") == 0) {
doroot:
    // wow! list ALL TAGS here!
    di = calloc(1, sizeof(*di));
    di->fcount = t_tag_count+spdcnt+2;
    di->items = calloc(di->fcount, sizeof(di->items[0]));
    //
    di->items[pos++].name = ":files";
    di->items[pos++].name = ":otags";
    //
    for (int f = 0; f < ARRAYLEN(spec_dirs); ++f) {
      if (has_flags[f]) continue;
      if (spec_dirs[f].type == SPT_TRANS || spec_dirs[f].type == SPT_ANY) {
        di->items[pos++].name = spec_dirs[f].name;
      }
    }
    for (uint32_t f = 0; f < t_tag_count; ++f) {
      if (get_str(get_tv(taglistt_data[f])->value)[0] == 0) {
        printf("tags=%u; idx=%u; ssize=%u; sofs=%u\n", tag_count, taglistt_data[f], string_bytes, get_tv(taglistt_data[f])->value);
        for (uint32_t fn = 0; fn < file_count; ++fn) {
          for (int tn = 0; tn < TFL_MAX; ++tn) {
            if (get_fi(fn)->tags[tn] == taglistt_data[f]) {
              printf("tag %d of file [%s] [%s] is empty!\n", tn, get_str(get_fi(fn)->realname), get_str(get_fi(fn)->shortname));
            }
          }
        }
        abort();
      }
      di->items[pos++].name = get_str(get_tv(taglistt_data[f])->value);
    }
    di->fcount = pos;
    goto ret_sort_di;
  }
  //
  if (strcmp(path, "/:otags") == 0 || strcmp(path, "/:otags/:ttags") == 0) {
    // wow! list ALL TAGS here!
    di = calloc(1, sizeof(*di));
    di->fcount = o_tag_count+spdcnt+2;
    di->items = calloc(di->fcount, sizeof(di->items[0]));
    //
    di->items[pos++].name = ":files";
    di->items[pos++].name = ":ttags";
    //
    for (int f = 0; f < ARRAYLEN(spec_dirs); ++f) {
      if (has_flags[f]) continue;
      if (spec_dirs[f].type == SPT_ORIG || spec_dirs[f].type == SPT_ANY) {
        di->items[pos++].name = spec_dirs[f].name;
      }
    }
    for (uint32_t f = 0; f < o_tag_count; ++f) di->items[pos++].name = get_str(get_tv(taglisto_data[f])->value);
    di->fcount = pos;
    goto ret_sort_di;
  }
  //
  sxdlogf("path: [%s]\n", path);
  // select and then refine
  while (*path) {
    fl_tagvalue_t *tv;
    while (*path == '/') ++path;
    if (!path[0]) break;
    {
      char *e = strchr(path, '/');
      if (e == NULL) {
        strcpy(condstr, path);
        path += strlen(path);
      } else {
        memcpy(condstr, path, e-path);
        condstr[e-path] = 0;
        path = e+1;
      }
    }
    sxdlogf("(fcount=%d) condstr: [%s]; sptype: %d\n", HASH_COUNT(fhash), condstr, sptype);
    if (condstr[0] == ':') {
      // specific tag
      if (strcmp(condstr, ":and") == 0 || strcmp(condstr, ":or") == 0) {
        if (firsttime) goto doroot;
        break;
      }
      firsttime = 0;
      //
      if (sdi != NULL) { sxdlogf("error: 2 specials\n"); HASH_CLEAR(hh, fhash); break; } // error
      //
      if (strcmp(condstr, ":files") == 0) {
        // we want only files
        onlyfiles = 1;
        if (files == NULL) {
          // wow, all files!
          files = malloc(sizeof(files[0])*file_count);
          for (uint32_t f = 0; f < file_count; ++f) {
            u32value_t *fl = &files[f];
            fl->idx = f;
            HASH_ADD_UINT32(fhash, idx, fl);
          }
        }
        break;
      }
      //
      if (strcmp(condstr, ":otags") == 0) {
        if (sptype == SPT_ORIG) { sxdlogf(" error: 'otags' in tmode\n"); HASH_CLEAR(hh, fhash); break; } // error
        sptype = SPT_ORIG;
        sxdlogf("otags; path=[%s]\n", path);
        continue;
      }
      //
      if (strcmp(condstr, ":ttags") == 0) {
        if (sptype == SPT_TRANS) { sxdlogf(" error: 'ttags' in tmode\n"); HASH_CLEAR(hh, fhash); break; } // error
        sptype = SPT_TRANS;
        sxdlogf("ttags; path=[%s]\n", path);
        continue;
      }
      //
      if ((sdi = get_special_by_name(condstr)) == NULL) {
        sxdlogf("unknown special: [%s]\n", condstr);
        HASH_CLEAR(hh, fhash);
        break;
      }
      //
      if (sdi->type != SPT_ANY && sptype != SPT_ANY) {
        if (sdi->type != sptype) {
          sxdlogf("bad special in mode %d: [%s]\n", sptype, condstr);
          HASH_CLEAR(hh, fhash);
          break;
        }
      }
      //
      sxdlogf("special: [%s] (fc=%d)\n", condstr, HASH_COUNT(fhash));
      if (sdi->type != SPT_ANY) sptype = sdi->type;
      //
      continue;
    }
    firsttime = 0;
    // got tag value
    tv = get_tag_with_special(condstr, sdi);
    if (tv == NULL) { sxdlogf(" for tag '%s': wtf?!\n", condstr); HASH_CLEAR(hh, fhash); break; } // no items, nothing can be found
    if (files == NULL) {
      // first time: add all files with this tag
      sxdlogf("first time: sdi=%p; sptype=%d", sdi, sptype);
      add_from_tv(tv, sdi, sptype);
    } else {
      // remove all files that have no such tag in given field
      sxdlogf("filtering out: sdi=%p; sptype=%d", sdi, sptype);
      remove_not_have_tv(tv, sdi, sptype);
    }
    sdi = NULL; // processed
  }
  // search complete
  //di->fcount += 2; // "." and ".."
  ++di->fcount; // :files
  if (!onlyfiles) {
    // specials
    di->fcount += spdcnt;
    if (sdi == NULL) {
      // path ends with tag value
      // build tag list
      sxdlogf("sptype=%d\n", sptype);
      if (!has_flags[0] || !has_flags[1]) { // no ':artist' or ':album'
        for (u32value_t *cf = fhash; cf != NULL; cf = cf->hh.next) {
          for (size_t sn = 0; sn < ARRAYLEN(spec_dirs); ++sn) {
            uint32_t idx;
            if (sptype != SPT_ANY && spec_dirs[sn].type != SPT_ANY && spec_dirs[sn].type != sptype) continue;
            if (only_albums) {
              if (sn != 1 && sn != 4) continue;
            }
            idx = get_fi(cf->idx)->tags[spec_dirs[sn].tflidx];
            HASH_FIND_UINT32(tl_hash, &idx, hx);
            if (hx == NULL) {
              hx = calloc(1, sizeof(*hx));
              hx->idx = idx;
              HASH_ADD_UINT32(tl_hash, idx, hx);
            }
          }
        }
        di->fcount += HASH_COUNT(tl_hash);
        if (opt_files_only_in_files) HASH_CLEAR(hh, fhash);
      } else {
        no_spfiles = 1;
      }
    } else {
      // path ends with tag name: collect all from field
      if (files == NULL) {
        // collect all from full list
        for (tagvalue_t *tv = tspec_hash[sdi->tflidx]; tv != NULL; tv = tv->hh.next) {
          uint32_t idx = tv2idx(tv->tv);
          HASH_FIND_UINT32(tl_hash, &idx, hx);
          if (hx == NULL) {
            hx = calloc(1, sizeof(*hx));
            hx->idx = idx;
            HASH_ADD_UINT32(tl_hash, idx, hx);
          }
        }
      } else {
        // collect from files
        //sxdlogf("sptype=%d; tag '%s'; fcount=%d\n", sptype, condstr, fcount);
        for (u32value_t *cf = fhash; cf != NULL; cf = cf->hh.next) {
          uint32_t idx = get_fi(cf->idx)->tags[sdi->tflidx];
          HASH_FIND_UINT32(tl_hash, &idx, hx);
          if (hx == NULL) {
            hx = calloc(1, sizeof(*hx));
            hx->idx = idx;
            HASH_ADD_UINT32(tl_hash, idx, hx);
          }
        }
      }
      di->fcount += HASH_COUNT(tl_hash);
      HASH_CLEAR(hh, fhash);
    }
  } else {
    // only files
    //di->fcount = HASH_COUNT(fhash);
  }
  //
  di->fcount += HASH_COUNT(fhash);
  di->items = calloc(di->fcount+1, sizeof(di->items[0]));
  sxdlogf("allocated %d items; pos=%d\n", di->fcount+1, pos);
  if (!onlyfiles) {
    // specials
    if (!no_spfiles) di->items[pos++].name = ":files";
    for (size_t f = 0; f < ARRAYLEN(spec_dirs); ++f) {
      if (has_flags[f]) continue;
      if (sptype == SPT_ANY || spec_dirs[f].type == SPT_ANY || spec_dirs[f].type == sptype) {
        di->items[pos++].name = spec_dirs[f].name;
      }
    }
    // tags
    HASH_ITER(hh, tl_hash, hx, thx) {
      di->items[pos++].name = get_str(get_tv(hx->idx)->value);
      HASH_DEL(tl_hash, hx);
      free(hx);
    }
  }
  // files
  if (HASH_COUNT(fhash) > 0) {
    for (u32value_t *cf = fhash; cf != NULL; cf = cf->hh.next) {
      if (pos >= di->fcount) { dlogf("FATAL: internal error in searcher!\n"); abort(); }
      di->items[pos].fidx = cf->idx+1; //NOTE +1!
      di->items[pos++].name = get_str(get_fi(cf->idx)->shortname);
    }
  }
  di->fcount = pos;
  HASH_CLEAR(hh, fhash);
  if (files != NULL) free(files);
  // sort files, so mplayer and others will see sorted dir
ret_sort_di:
  qsort(di->items, di->fcount, sizeof(di->items[0]), lambda(int, (const void *p0, const void *p1) {
    const diritem_t *i0 = (const diritem_t *)p0;
    const diritem_t *i1 = (const diritem_t *)p1;
    int sgn;
    const fl_fileinfo_t *f0;
    const fl_fileinfo_t *f1;
    //sxdlogf("[%s] [%s]\n", s0, s1);
    if (i0->name[0] == ':' && i1->name[0] != ':') return -1;
    if (i0->name[0] != ':' && i1->name[0] == ':') return 1;
    if (i0->fidx == 0) return (i1->fidx != 0 ? 1 : strcmp(i0->name, i1->name));
    if (i1->fidx == 0) return (i0->fidx != 0 ? -1 : strcmp(i0->name, i1->name));
    // compare files
    f0 = get_fi(i0->fidx-1);
    f1 = get_fi(i1->fidx-1);
    // years
    if (f0->year != f1->year && (f0->year != 0 || f1->year != 0)) {
      sgn = zdifsign(f0->year, f1->year);
      if (sgn) return sgn;
    }
    // artists
    sgn = zdifsign(f0->artist_t, f1->artist_t);
    if (sgn) return sgn;
    // albums
    sgn = zdifsign(f0->album_t, f1->album_t);
    if (sgn) return sgn;
    // track numbers
    sgn = zdifsign(f0->tracknum, f1->tracknum);
    if (sgn) return sgn;
    // names
    return strcmp(i0->name, i1->name);
  }));
  return di;
}


static dirinfo_t *list_dir_or (char *pt) {
  for (;;) {
    dirinfo_t *di;
    int stop = 0;
    char *ta = strstr(pt, "/:or/");
    if (ta == NULL) return list_dir_simple(pt);
    *ta = 0;
    sxdlogf("OR: [%s]\n", pt);
    di = list_dir_simple(pt);
    *ta = '/';
    pt = strchr(ta+1, '/');
    // check if we have something except specials
    for (int f = 0; f < di->fcount; ++f) if (di->items[f].name[0] != ':') { stop = 1; break; }
    if (stop) return di;
    free(di->items);
    free(di);
  }
}


typedef struct {
  //fl_fileinfo_t **files;
  const char *name; // short names
  UT_hash_handle hh;
} nlist_t;


static dirinfo_t *list_dir (const char *path) {
  char *pt;
  nlist_t *nhash = NULL, *nm, *nmtmp;
  dirinfo_t *res;

  void fill_nhash (dirinfo_t *di) {
    if (di != NULL) {
      for (int f = 0; f < di->fcount; ++f) {
        nlist_t *nm;
        HASH_FIND_STR(nhash, di->items[f].name, nm);
        if (nm == NULL) {
          nm = calloc(1, sizeof(*nm));
          nm->name = di->items[f].name;
          HASH_ADD_KEYPTR(hh, nhash, nm->name, strlen(nm->name), nm);
        }
      }
      free(di->items);
      free(di);
    }
  }

  if (strstr(path, "/:and/") == NULL && strstr(path, "/:or/") == NULL) return list_dir_simple(path);
  pt = alloca(strlen(path)+1);
  strcpy(pt, path);
  for (;;) {
    char *tn;
    sxdlogf("***a: [%s]\n", pt);
    tn = strstr(pt, "/:and/");
    if (tn == NULL) {
      sxdlogf("ALAST: [%s]\n", pt);
      fill_nhash(list_dir_or(pt));
      break;
    }
    *tn = 0;
    sxdlogf("AND: pt=[%s]; tn=[/%s]\n", pt, tn+1);
    fill_nhash(list_dir_or(pt));
    pt = strchr(tn+1, '/');
    sxdlogf("xxx: [%s]\n", pt);
  }
  // now build result from nhash
  HASH_SORT(nhash, lambda(int, (const nlist_t *i0, const nlist_t *i1) {
    if (i0->name[0] == ':' && i1->name[0] != ':') return -1;
    if (i0->name[0] != ':' && i1->name[0] == ':') return 1;
    return strcmp(i0->name, i1->name);
  }));
  res = calloc(1, sizeof(*res));
  res->items = calloc(HASH_COUNT(nhash)+1, sizeof(res->items[0]));
  HASH_ITER(hh, nhash, nm, nmtmp) {
    res->items[res->fcount++].name = nm->name;
    HASH_DEL(nhash, nm);
    free(nm);
  }
  return res;
}
