/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
//#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "k8clock.h"


k8clock_type k8clock_initialized = K8CLOCK_ERROR;
k8clock_t clk;


#ifndef CLOCK_MONOTONIC_RAW
# define K8CLOCK_CLOCK_TYPE  CLOCK_MONOTONIC_RAW
#else
# define K8CLOCK_CLOCK_TYPE  CLOCK_MONOTONIC
#endif


static k8clock_type k8clock_check_source (void) {
  char buf[128], *p, *e;
  int fd = open("/sys/devices/system/clocksource/clocksource0/current_clocksource", O_RDONLY);
  if (fd < 0) {
    //fprintf(stderr, "WARNING: can't determine clock source!\n");
    return K8CLOCK_OTHER;
  }
  memset(buf, 0, sizeof(buf));
  read(fd, buf, sizeof(buf)-1);
  close(fd);
  if ((e = strchr(buf, '\n')) == NULL) e = buf+strlen(buf);
  for (p = buf; p < e && (unsigned char)(p[0]) <= 32; ++p) ;
  *e = 0;
  //fprintf(stderr, "[%s]\n", p);
  return (strcmp(p, "hpet") == 0 ? K8CLOCK_HPET : K8CLOCK_OTHER);
}


__attribute__((constructor)) static void k8clock_ctor (void) {
  if (k8clock_initialized == K8CLOCK_ERROR) {
    struct timespec cres;
    k8clock_initialized = k8clock_check_source();
    if (clock_getres(K8CLOCK_CLOCK_TYPE, &cres) != 0) {
      //fprintf(stderr, "ERROR: can't get clock resolution!\n");
      k8clock_initialized = K8CLOCK_ERROR;
      return;
    }
    //fprintf(stderr, "CLOCK_MONOTONIC: %ld:%ld\n", cres.tv_sec, cres.tv_nsec);
    if (cres.tv_sec > 0 || cres.tv_nsec > (long)1000000*10 /*10 ms*/) {
      //fprintf(stderr, "ERROR: real-time clock resolution is too low!\n");
      k8clock_initialized = K8CLOCK_ERROR;
      return;
    }
    k8clock_init(&clk);
  }
}


void k8clock_init (k8clock_t *clk) {
  if (clk != NULL && k8clock_initialized != K8CLOCK_ERROR) {
    if (clock_gettime(K8CLOCK_CLOCK_TYPE, &clk->stt) != 0) {
      //fprintf(stderr, "ERROR: can't get real-time clock value!\n");
    }
  }
}


uint64_t k8clock_ms (k8clock_t *clk) {
  if (clk != NULL && k8clock_initialized != K8CLOCK_ERROR) {
    struct timespec ts;
    if (clock_gettime(K8CLOCK_CLOCK_TYPE, &ts) != 0) {
      //fprintf(stderr, "ERROR: can't get real-time clock value!\n");
      return 0;
    }
    // ah, ignore nanoseconds in clk->stt here: we need only 'differential' time, and it can start with something weird
    return ((int64_t)(ts.tv_sec-clk->stt.tv_sec))*1000+(ts.tv_nsec-clk->stt.tv_nsec)/1000000+1;
  }
  return 0;
}


uint64_t k8clock_micro (k8clock_t *clk) {
  if (clk != NULL && k8clock_initialized != K8CLOCK_ERROR) {
    struct timespec ts;
    if (clock_gettime(K8CLOCK_CLOCK_TYPE, &ts) != 0) {
      //fprintf(stderr, "ERROR: can't get real-time clock value!\n");
      return 0;
    }
    // ah, ignore nanoseconds in clk->stt here: we need only 'differential' time, and it can start with something weird
    return ((int64_t)(ts.tv_sec-clk->stt.tv_sec))*1000000+(ts.tv_nsec-clk->stt.tv_nsec)/1000+1;
  }
  return 0;
}


uint64_t k8clock (void) {
  return k8clock_ms(&clk);
}


uint64_t k8clockmicro (void) {
  return k8clock_micro(&clk);
}
