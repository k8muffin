/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _TRANSLIT_H_
#define _TRANSLIT_H_


// free() the result (it will never be NULL)
// also converts to lower case
extern char *translitstr (const char *src);


/****************************************************************************/
/* char up/down                                                             */
/****************************************************************************/
/* koi8-u */
static inline int le2lower (int ch) {
  ch &= 0xff;
  if (ch < 128) {
    if (ch >= 'A' && ch <= 'Z') ch += 32;
  } else {
    if (ch >= 224 && ch <= 255) ch -= 32;
    else {
      switch (ch) {
        case 179: case 180: case 182: case 183: case 189: ch -= 16; break;
      }
    }
  }
  return ch;
}


static inline int le2upper (int ch) {
  ch &= 0xff;
  if (ch < 128) {
    if (ch >= 'a' && ch <= 'z') ch -= 32;
  } else {
    if (ch >= 192 && ch <= 223) ch += 32;
    else {
      switch (ch) {
        case 163: case 164: case 166: case 167: case 173: ch += 16; break;
      }
    }
  }
  return ch;
}


static inline int leIsAlpha (int ch) {
  ch &= 0xff;
  if (ch >= 'A' && ch <= 'Z') return 1;
  if (ch >= 'a' && ch <= 'z') return 1;
  if (ch >= 192) return 1;
  switch (ch) {
    case 163: case 164: case 166: case 167: case 173:
    case 179: case 180: case 182: case 183: case 189:
      return 1;
  }
  return 0;
}



static inline int leIsDigit (int ch) { return ch >= '0' && ch <= '9'; }
static inline int leIsXDigit (int ch) { ch = le2upper(ch); return leIsDigit(ch) || (ch >= 'A' && ch <= 'F'); }
static inline int leIsAlNum (int ch) { return leIsDigit(ch) || leIsAlpha(ch); }
static inline int leIsUpper (int ch) { return leIsAlpha(ch) && ch == le2upper(ch); }
static inline int leIsLower (int ch) { return leIsAlpha(ch) && ch == le2lower(ch); }
static inline int leIsSpace (int ch) { return (ch && (ch&0xff) <= 32); }


#endif
