/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif

//#include <ctype.h>
#include <dirent.h>
#include <iconv.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <tag_c.h>

#include "common.h"

#include "translit.h"


////////////////////////////////////////////////////////////////////////////////
static char *trimstr (char *s) {
  char *res = s;
  if (s != NULL && s[0]) {
    //char *olds = strdup(s), *xs = s;
    char *ns, *p;
    // change all '/' to underlines
    for (ns = s; *ns; ++ns) if (*ns == '/' || *ns == 127) *ns = '_';
    // remove duplicate underlines
    for (ns = p = s; *ns; ++ns) {
      if (*ns == '_') {
        if (p > s && p[-1] != '_') *p++ = *ns;
      } else {
        *p++ = *ns;
      }
    }
    *p = 0;
    // remove duplicate spaces
    for (ns = p = s; *ns; ++ns) {
      if (leIsSpace(*ns)) {
        if (p > s && !leIsSpace(p[-1])) *p++ = *ns;
      } else {
        *p++ = *ns;
      }
    }
    *p = 0;
    // trim trailing spaces and underlines
    ns = s+strlen(s)-1;
    while (ns >= s && (leIsSpace(*ns) || *ns == '_')) --ns;
    ns[1] = 0;
    // trim leading spaces and underlines
    for (ns = s; *ns && (leIsSpace(*ns) || *ns == '_'); ++ns) ;
    if (ns != s) memmove(s, ns, strlen(ns)+1);
  }
  return res;
}


////////////////////////////////////////////////////////////////////////////////
#define xstrdup(ss)  ({ \
  const char *s = ss; \
  char *d = alloca(s != NULL ? strlen(s)+1 : 1); \
  if (s != NULL) strcpy(d, s); \
  d; \
})

static int show_tags (const char *filename, int donl) {
  int res = 0;
  TagLib_Tag *tag;
  //const TagLib_AudioProperties *properties;
  TagLib_File *file;
  taglib_set_strings_unicode(1);
  file = taglib_file_new(filename);
  if (file == NULL) return 0;
  tag = taglib_file_tag(file);
  if (tag != NULL) {
    char *artist = trimstr(xstrdup(taglib_tag_artist(tag)));
    char *album = trimstr(xstrdup(taglib_tag_album(tag)));
    char *title = trimstr(xstrdup(taglib_tag_title(tag)));
    char *genre = trimstr(xstrdup(taglib_tag_genre(tag)));
    unsigned int yy = taglib_tag_year(tag);
    unsigned int tno = taglib_tag_track(tag);
    //
    if (yy > 0) {
      if (yy < 30) yy += 2000;
      else if (yy < 100) yy += 1900;
      else if (yy < 1930) yy = 0;
    }
    if (tno == 0 || tno > 99) tno = 0;
    if (artist[0] || album[0] || title[0] || genre[0] || yy || tno) {
      if (donl) printf("\n");
      if (artist[0]) printf("ARTIST=%s\n", artist);
      if (album[0]) printf("ALBUM=%s\n", album);
      if (title[0]) printf("TITLE=%s\n", title);
      if (genre[0]) printf("GENRE=%s\n", genre);
      if (yy) printf("YEAR=%u\n", yy);
      if (tno) printf("TRACKNUMBER=%u\n", tno);
      res = 1;
    }
    // fuck comments
  }
  //
  /*
  properties = taglib_file_audioproperties(file);
  if (properties != NULL) {
    int seconds = taglib_audioproperties_length(properties)%60;
    int minutes = (taglib_audioproperties_length(properties)-seconds)/60;
    printf("-- AUDIO --\n");
    printf("bitrate    : %d\n", taglib_audioproperties_bitrate(properties));
    printf("sample rate: %d\n", taglib_audioproperties_samplerate(properties));
    printf("channels   : %d\n", taglib_audioproperties_channels(properties));
    printf("length     : %d:%02i\n", minutes, seconds);
  }
  */
  //
  taglib_tag_free_strings();
  taglib_file_free(file);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  int donl = 0;
  for (int f = 1; f < argc; ++f) {
    if (show_tags(argv[f], donl) > 0) donl = 1;
  }
  //
  return 0;
}
