/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <ctype.h>
#include <dirent.h>
#include <iconv.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "libdbg/dbglog.h"
#include "translit.h"

#include "uthash.h"
#include "libk8clock/k8clock.h"


////////////////////////////////////////////////////////////////////////////////
#include "tagload.c"
#include "search.c"


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  uint64_t stt, ste;
  dbglog_set_screenout(1);
  dbglog_set_fileout(0);
  //
  printf("loading database...\n");
  stt = k8clock();
  if (tagdb_load("tags.dat") != 0) { fprintf(stderr, "FATAL: can't open tagfile!\n"); return -1; }
  ste = k8clock();
  printf("database load time: %.15g seconds\n", (double)(ste-stt)/1000.0);
  printf("building hashes...\n");
  stt = k8clock();
  build_hashes();
  ste = k8clock();
  printf("hash build time: %.15g seconds\n", (double)(ste-stt)/1000.0);
  if (argc > 1) {
    for (int f = 1; f < argc; ++f) {
      dirinfo_t *di = list_dir(argv[f]);
      // dump
      if (di != NULL) {
        dlogf("=====================\n");
        for (int f = 0; f < di->fcount; ++f) fprintf(stderr, "%s\n", di->items[f].name);
        free(di->items);
        free(di);
      }
    }
  } else {
    char s[128];
    fgets(s, sizeof(s), stdin);
  }
  clear_hashes();
  tagdb_unload();
  return 0;
}
