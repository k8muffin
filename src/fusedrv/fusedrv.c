/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#define FUSE_USE_VERSION  26

#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <iconv.h>
#include <libgen.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/xattr.h>

#include "../libdbg/dbglog.h"
#include "../uthash.h"
#include "../translit.h"
#include "../libk8clock/k8clock.h"


////////////////////////////////////////////////////////////////////////////////
#define NO_SEARCH_DBGLOG
//#define FUSEDRV_LOG_CALLS


////////////////////////////////////////////////////////////////////////////////
static int opt_log_calls = 0;

# define fd_call_dlogf(...)  do { \
  if (opt_log_calls) dlogf(__VA_ARGS__); \
} while (0)


////////////////////////////////////////////////////////////////////////////////
#include "../tagload.c"
#include "../search.c"


////////////////////////////////////////////////////////////////////////////////
static char *tagfilename = NULL;
static int tfs_dirs_opened = 0; // # of opened dirs
static int tfs_need_reload = 0; // !0: reload when last dir closed
static uint64_t cur_dir_time = 1;


////////////////////////////////////////////////////////////////////////////////
static inline void tagdb_check_and_reload (void) {
  if (tfs_need_reload) {
    if (tfs_dirs_opened == 0) {
      dlogf("* reloading tagdb from '%s'...", tagfilename);
      tfs_need_reload = 0;
      clear_hashes();
      dlogf("* hashes cleared");
      tagdb_unload();
      dlogf("* tagdb unloaded");
      if (tagdb_load(tagfilename) != 0) {
        dlogf("FATAL: can't load tagfile: '%s'!\n", tagfilename);
        abort();
      }
      dlogf("* tagdb loaded");
      build_hashes();
      dlogf("* hashes built");
      ++cur_dir_time;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static char *get_real_path (const char *fn, int add_slash) {
  if (fn != NULL && fn[0]) {
    char *pp = realpath(fn, NULL);
    if (pp != NULL) {
      if (pp[0]) {
        if (add_slash && pp[strlen(pp)-1] != '/') {
          char *t = malloc(strlen(pp)+2);
          sprintf(t, "%s/", pp);
          free(pp);
          pp = t;
        }
        return pp;
      }
      free(pp);
    }
  }
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
//#define CTR ((my_data_struct_t *)(fuse_get_context()->private_data))


// report errors to logfile and give -errno to caller
/*
static int tfs_error (const char *str) {
  int ret = -errno;
  fd_call_dlogf("  ERROR %s: %s", str, strerror(errno));
  return ret;
}
*/


static const fl_tagvalue_t *find_by_tag_name (const char *name) {
  const char *t = strrchr(name, '/');
  if (t == NULL) t = name; else ++t;
  return get_tag_by_name(name, &tval_hash);
}


// short names are unique
static fl_fileinfo_t *find_by_short_name (const char *path) {
  filename_t *sf;
  const char *t = strrchr(path, '/');
  if (t == NULL) t = path; else ++t;
  HASH_FIND_STR(sname_hash, t, sf);
  return (sf != NULL ? sf->fi : NULL);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int fd; // <0: internal file
  int space_eater;
  int size;
  char *data;
  //UT_hash_handle hh;
} filehandle_t;


//static filehandle_t *fhandles = NULL;


static filehandle_t *colon_create_info_file (const char *path) {
  filehandle_t *fh = NULL;
  fd_call_dlogf("colon_create_info_file: [%s]\n", path);
  if (strcmp(path, "/:/info") == 0) {
    fh = calloc(1, sizeof(*fh));
    fh->fd = -1;
    fh->data = calloc(8192, 1);
    //snprintf(fh->data, 8192, "files: %d; otags: %d; ntags: %d\n", HASH_COUNT(file_name_hash), HASH_COUNT(tagv_hash_o), HASH_COUNT(tagv_hash_t));
    snprintf(fh->data, 8192, "files: %d; tags: %d; opened dirs: %d; reload deferred: %d\n", HASH_COUNT(sname_hash), HASH_COUNT(tval_hash),
       tfs_dirs_opened, tfs_need_reload
    );
    fh->size = strlen(fh->data);
  }
  return fh;
}


static filehandle_t *fht_open (const char *realpath) {
  int fd = open(realpath, O_RDONLY);
  if (fd >= 0) {
    filehandle_t *fh = calloc(1, sizeof(*fh));
    fh->fd = fd;
    return fh;
  }
  return NULL;
}


static filehandle_t *fht_create_eater (const char *realpath) {
  filehandle_t *fh = calloc(1, sizeof(*fh));
  fh->fd = -1;
  fh->space_eater = 1;
  return fh;
}


static void fht_close (filehandle_t *fh) {
  if (fh != NULL) {
    if (fh->fd >= 0) close(fh->fd);
    if (fh->data != NULL) free(fh->data);
    free(fh);
  }
}


static int fht_read (const char *path, char *buf, size_t size, off_t offset, filehandle_t *fh) {
  if (fh != NULL) {
    if (fh->fd >= 0) {
      ssize_t rd;
      fd_call_dlogf("tfs_read: path=[%s], buf=0x%p, size=%d, offset=%lld", path, buf, size, offset);
      if (fh->fd < 0) return -EPERM;
      if (offset >= 0x7fffffff || size >= 0x7fffffff) return 0;/*-EPERM;*/ //???
      if (lseek(fh->fd, offset, SEEK_SET) == (off_t)-1) return -EPERM;
      rd = read(fh->fd, buf, size);
      if (rd < 0) return -EPERM;
      return rd;
    }
    if (fh->data != NULL) {
      if (offset >= fh->size) return 0;
      if ((uint64_t)offset+size > fh->size) size = fh->size-offset;
      if (size == 0) return 0;
      memcpy(buf, fh->data+offset, size);
      return size;
    }
  }
  return -EPERM;
}


static int fht_write (const char *path, const char *buf, size_t size, off_t offset, filehandle_t *fh) {
  if (fh != NULL) {
    if (fh->fd < 0) {
      if (fh->space_eater) return size; // eat 'em alive!
    }
  }
  return -EPERM;
}


////////////////////////////////////////////////////////////////////////////////
// this is used in getattr() and readdir()
// note that readdir() will call this with 'filename.ext' and getattr() will call this with full path
static const char *spxnames[] = {
  ":otags",
  ":ttags",
  ":files",
  ":or",
  ":and",
};

static int do_stat (const char *path, struct stat *statbuf) {
  fl_fileinfo_t *nfo;
  const char *name;
  memset(statbuf, 0, sizeof(*statbuf));
  //
  fd_call_dlogf("do_stat: [%s]\n", path);
  //FIXME!
  statbuf->st_uid = fuse_get_context()->uid;
  statbuf->st_gid = fuse_get_context()->gid;
  //
  //TODO: set to 2? do we rally want 'find' to work on muffin?
  statbuf->st_nlink = 1; // was '2'; actually, this should be set to 'subdir_count+2' (or 1), else 'find' will fail
  statbuf->st_mode = S_IFDIR|0555;
  //statbuf->st_dev = get_fi(0)->devid;
  //statbuf->st_ino = get_fi(0)->inode;
  statbuf->st_blksize = 4096; // i/o block size
  //statbuf->st_blocks = 1;
  //statbuf->st_size = 1;
  statbuf->st_atime = statbuf->st_ctime = statbuf->st_mtime = cur_dir_time;
  //
  //stat(get_str(get_fi(0)->realname), statbuf);
  //stat("/home/ketmar", statbuf);
  statbuf->st_mode = S_IFDIR|0555;
  //
  if (strcmp(path, "/") == 0) {
    return 0;
  }
  if (strcmp(path, "/:") == 0) {
    statbuf->st_mode = S_IFDIR|0755; // writeable
    return 0;
  }
  if (strncmp(path, "/:/", 3) == 0) {
    // all files are regular ones here
    statbuf->st_mode = S_IFREG|0444;
    if (strcmp(path, "/:/reload") == 0) {
      if (tfs_need_reload) {
        fd_call_dlogf(" stat: eater is here\n");
        statbuf->st_mode = S_IFREG|0777;
        return 0;
      }
      return -ENOENT;
    }
    statbuf->st_size = 4096; // arbitrary, but must be bigger than the actual 'info' files
    return 0;
  }
  //
  name = strrchr(path, '/');
  if (name == NULL) name = path; else ++name;
  fd_call_dlogf(" do_stat_name: [%s]\n", name);
  //
  if (name[0] == ':') {
    fd_call_dlogf(" do_spec_stat: [%s]\n", name);
    for (size_t f = 0; f < ARRAYLEN(spxnames); ++f) if (strcmp(name, spxnames[f]) == 0) {
      fd_call_dlogf(" stat: SPECIAL\n");
      return 0;
    }
    for (size_t f = 0; f < ARRAYLEN(spec_dirs); ++f) if (strcmp(spec_dirs[f].name, name) == 0) {
      fd_call_dlogf(" stat: SPECIAL\n");
      return 0;
    }
    dlogf("baddir: [%s]\n", path);
    return -ENOENT;
  }
  //
  if ((nfo = find_by_short_name(name)) != NULL) {
    // normal file
    /*
    if (do_real_stat) {
      if (stat(nfo->fullname, statbuf) == 0) return 0;
      return -ENOENT; //TODO: return better error here
    }
    */
    fd_call_dlogf(" stat: file\n");
#ifdef USE_DEVID_INODE
    statbuf->st_dev = nfo->devid;
    statbuf->st_ino = nfo->inode;
#else
    statbuf->st_dev = 666;
    statbuf->st_ino = (uintptr_t)nfo;
#endif
    statbuf->st_mode = S_IFREG|0444;
    statbuf->st_size = nfo->size;
    statbuf->st_blksize = 4096; // i/o block size
    statbuf->st_blocks = (nfo->size/512)+(nfo->size%512 ? 1 : 0);
    statbuf->st_atime = statbuf->st_ctime = statbuf->st_mtime = nfo->mtime;
    return 0;
  }
  //
  //dlogf("not-a-file: [%s]\n", path);
  if (find_by_tag_name(name) != NULL) {
    // this is dirs too
    fd_call_dlogf(" stat: dir\n");
    return 0;
  }
  //
  dlogf(" stat: ***not-found: [%s]\n", path);
  return -ENOENT;
}


/** Get file attributes.
 *
 * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
 * ignored.  The 'st_ino' field is ignored except if the 'use_ino'
 * mount option is given.
 */
static int tfs_getattr (const char *path, struct stat *statbuf) {
  fd_call_dlogf("tfs_getattr: path=[%s]", path);
  return do_stat(path, statbuf);
}


/** Read the target of a symbolic link
 *
 * The buffer should be filled with a null terminated string.  The
 * buffer size argument includes the space for the terminating
 * null character.  If the linkname is too long to fit in the
 * buffer, it should be truncated.  The return value should be 0
 * for success.
 */
// Note the system readlink() will truncate and lose the terminating
// null.  So, the size passed to to the system readlink() must be one
// less than the size passed to tfs_readlink()
static int tfs_readlink (const char *path, char *link, size_t size) {
  fd_call_dlogf("tfs_readlink: path=[%s]", path);
  return -ENOENT; // there are no symlinks
}


/** Create a file node
 *
 * This is called for creation of all non-directory, non-symlink
 * nodes.  If the filesystem defines a create() method, then for
 * regular files that will be called instead.
 */
static int tfs_mknod (const char *path, mode_t mode, dev_t dev) {
  fd_call_dlogf("tfs_mknod: path=[%s], mode=0%03o, dev=%lld", path, mode, dev);
  //
  //if (!S_ISREG(mode)) return -EINVAL; // only regular files can be created
  // no new files can be created here
  //return (exists ? EEXIST : EFAULT);
  return -EFAULT;
}


/** Create a directory
 *
 * Note that the mode argument may not have the type specification
 * bits set, i.e. S_ISDIR(mode) can be false.  To obtain the
 * correct directory type bits use  mode|S_IFDIR
 */
static int tfs_mkdir (const char *path, mode_t mode) {
  fd_call_dlogf("tfs_mkdir: path=[%s], mode=0%03o", path, mode);
  // no new directories can be created here
  return -EFAULT;
}


/** Remove a file */
static int tfs_unlink (const char *path) {
  fd_call_dlogf("tfs_unlink: path=[%s]", path);
  return -EFAULT;
}


/** Remove a directory */
static int tfs_rmdir (const char *path) {
  fd_call_dlogf("tfs_rmdir: path=[%s]", path);
  return -EFAULT;
}


/** Create a symbolic link */
// The parameters here are a little bit confusing, but do correspond
// to the symlink() system call.  The 'path' is where the link points,
// while the 'link' is the link itself.  So we need to leave the path
// unaltered, but insert the link into the mounted directory.
static int tfs_symlink (const char *path, const char *link) {
  fd_call_dlogf("tfs_symlink: path=[%s]; link=[%s]", path, link);
  return -EFAULT;
}


/** Rename a file */
// both path and newpath are fs-relative
static int tfs_rename (const char *path, const char *newpath) {
  fd_call_dlogf("tfs_rename: path=[%s]; newpath=[%s]", path, newpath);
  return -EFAULT;
}


/** Create a hard link to a file */
static int tfs_link (const char *path, const char *newpath) {
  fd_call_dlogf("tfs_link: path=[%s]; newpath=[%s]", path, newpath);
  return -EFAULT;
}


/** Change the permission bits of a file */
static int tfs_chmod (const char *path, mode_t mode) {
  fd_call_dlogf("tfs_chmod: path=[%s], mode=0%03o", path, mode);
  return 0;
}


/** Change the owner and group of a file */
static int tfs_chown (const char *path, uid_t uid, gid_t gid) {
  fd_call_dlogf("tfs_chown: path=[%s], uid=%d, gid=%d", path, uid, gid);
  return 0;
}


/** Change the size of a file */
static int tfs_truncate (const char *path, off_t newsize) {
  fd_call_dlogf("tfs_truncate: path=[%s], newsize=%lld", path, newsize);
  return -EFAULT;
}


/** Change the access and/or modification times of a file
 *
 * Deprecated, use utimens() instead.
 */
/*
static int tfs_utime (const char *path, struct utimbuf *ubuf) {
  return 0;
}
*/


/** File open operation
 *
 * No creation (O_CREAT, O_EXCL) and by default also no
 * truncation (O_TRUNC) flags will be passed to open(). If an
 * application specifies O_TRUNC, fuse first calls truncate()
 * and then open(). Only if 'atomic_o_trunc' has been
 * specified and kernel version is 2.6.24 or later, O_TRUNC is
 * passed on to open.
 *
 * Unless the 'default_permissions' mount option is given,
 * open should check if the operation is permitted for the
 * given flags. Optionally open may also return an arbitrary
 * filehandle in the fuse_file_info structure, which will be
 * passed to all file operations.
 *
 * Changed in version 2.2
 */
static int tfs_open (const char *path, struct fuse_file_info *fi) {
  fl_fileinfo_t *nfo;
  filehandle_t *fh = NULL;
  fd_call_dlogf("tfs_open: path=[%s], fi=0x%p; append=%d; trunc=%d\n", path, fi, (fi->flags&O_APPEND ? 1 : 0), (fi->flags&O_TRUNC ? 1 : 0));
  if ((fi->flags&O_APPEND) || (fi->flags&O_TRUNC)) {
    fd_call_dlogf(" bad flags!\n");
    return -EPERM;
  }
  switch (fi->flags&(O_RDONLY|O_WRONLY|O_RDWR)) {
    case O_RDONLY: break;
    case O_WRONLY:
    case O_RDWR:
      fd_call_dlogf(" bad access mode!\n");
      return -EPERM;
  }
  //
  if (strncmp(path, "/:/", 3) == 0) {
    fh = colon_create_info_file(path);
  } else {
    if ((nfo = find_by_short_name(path)) != NULL) {
      fd_call_dlogf(" found: [%s]\n", get_str(nfo->realname));
      fh = fht_open(get_str(nfo->realname));
    }
  }
  if (fh != NULL) {
    fi->fh = (uintptr_t)fh;
    return 0;
  }
  fd_call_dlogf(" shit!\n");
  return -ENOENT;
}


/** Read data from an open file
 *
 * Read should return exactly the number of bytes requested except
 * on EOF or error, otherwise the rest of the data will be
 * substituted with zeroes.  An exception to this is when the
 * 'direct_io' mount option is specified, in which case the return
 * value of the read system call will reflect the return value of
 * this operation.
 *
 * Changed in version 2.2
 */
static int tfs_read (const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  filehandle_t *fh = (filehandle_t *)(uintptr_t)(fi->fh);
  return fht_read(path, buf, size, offset, fh);
}


/** Write data to an open file
 *
 * Write should return exactly the number of bytes requested
 * except on error.  An exception to this is when the 'direct_io'
 * mount option is specified (see read operation).
 *
 * Changed in version 2.2
 */
static int tfs_write (const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  fd_call_dlogf("tfs_write: path=[%s], buf=0x%p, size=%d, offset=%lld, fi=0x%p", path, buf, size, offset, fi);
  filehandle_t *fh = (filehandle_t *)(uintptr_t)(fi->fh);
  return fht_write(path, buf, size, offset, fh);
}


/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
static int tfs_statfs (const char *path, struct statvfs *statv) {
  return -EPERM;
  //
  //statvfs
  //memset(statv, 0, sizeof(*statv));
  //statv->f_bsize = 8192; // fixme
  fd_call_dlogf("tfs_statfs: path=[%s], statv=0x%p", path, statv);
  //return retstat;
}


/** Possibly flush cached data
 *
 * BIG NOTE: This is not equivalent to fsync().  It's not a
 * request to sync dirty data.
 *
 * Flush is called on each close() of a file descriptor.  So if a
 * filesystem wants to return write errors in close() and the file
 * has cached dirty data, this is a good place to write back data
 * and return any errors.  Since many applications ignore close()
 * errors this is not always useful.
 *
 * NOTE: The flush() method may be called more than once for each
 * open().  This happens if more than one file descriptor refers
 * to an opened file due to dup(), dup2() or fork() calls.  It is
 * not possible to determine if a flush is final, so each flush
 * should be treated equally.  Multiple write-flush sequences are
 * relatively rare, so this shouldn't be a problem.
 *
 * Filesystems shouldn't assume that flush will always be called
 * after some writes, or that if will be called at all.
 *
 * Changed in version 2.2
 */
static int tfs_flush (const char *path, struct fuse_file_info *fi) {
  fd_call_dlogf("tfs_flush: path=[%s], fi=0x%p", path, fi);
  return 0;
}


/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.  It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
static int tfs_release (const char *path, struct fuse_file_info *fi) {
  filehandle_t *fh = (filehandle_t *)(uintptr_t)(fi->fh);
  int is_eater = (fh != NULL ? fh->space_eater : 0);
  fd_call_dlogf("tfs_release: path=[%s]; fh=%p; eater=%d\n", path, fh, is_eater);
  fht_close(fh);
  fi->fh = 0;
  if (is_eater) tagdb_check_and_reload();
  return 0;
}


/** Synchronize file contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data.
 *
 * Changed in version 2.2
 */
static int tfs_fsync (const char *path, int datasync, struct fuse_file_info *fi) {
  fd_call_dlogf("tfs_fsync: path=[%s], datasync=%d, fi=0x%p", path, datasync, fi);
  return 0;
}


/** Set extended attributes */
static int tfs_setxattr (const char *path, const char *name, const char *value, size_t size, int flags) {
  fd_call_dlogf("tfs_setxattr: path=[%s], name=[%s], value=[%s], size=%d, flags=0x%08x", path, name, value, size, (unsigned int)flags);
  return -EPERM;
}


static const struct {
  const char *name;
  int tflidx;
  int hidden;
} xattr_tags[] = {
  {.name="user.oartist", .tflidx=TFL_ARTIST_O, .hidden=1},
  {.name="user.artist", .tflidx=TFL_ARTIST_O, .hidden=0},
  {.name="user._artist", .tflidx=TFL_ARTIST_T, .hidden=0},
  {.name="user.tartist", .tflidx=TFL_ARTIST_T, .hidden=1},
  {.name="user.oalbum", .tflidx=TFL_ALBUM_O, .hidden=1},
  {.name="user.album", .tflidx=TFL_ALBUM_O, .hidden=0},
  {.name="user._album", .tflidx=TFL_ALBUM_T, .hidden=0},
  {.name="user.talbum", .tflidx=TFL_ALBUM_T, .hidden=1},
  {.name="user.otitle", .tflidx=TFL_TITLE_O, .hidden=1},
  {.name="user.title", .tflidx=TFL_TITLE_O, .hidden=0},
  {.name="user._title", .tflidx=TFL_TITLE_T, .hidden=0},
  {.name="user.ttitle", .tflidx=TFL_TITLE_T, .hidden=1},
  {.name="user.ogenre", .tflidx=TFL_GENRE_O, .hidden=1},
  {.name="user.genre", .tflidx=TFL_GENRE_O, .hidden=0},
  {.name="user._genre", .tflidx=TFL_GENRE_T, .hidden=0},
  {.name="user.tgenre", .tflidx=TFL_GENRE_T, .hidden=1},
  {.name="user.year", .tflidx=TFL_YEAR, .hidden=0},
};


/** Get extended attributes */
// If the given node described by path has the named extended attribute, and the vlen is zero,
// return the number of bytes required to provide the value of the extended attribute.
// The next call will have a buffer that is long enough to hold the value. Copy the value into
// the buffer and return the number of bytes that the value occupies.
static int tfs_getxattr (const char *path, const char *name, char *value, size_t size) {
  fd_call_dlogf("tfs_getxattr: path=[%s], name=[%s], value=0x%p, size=%d", path, name, value, size);
  if (name != NULL && name[0]) {
    fl_fileinfo_t *nfo;
    const char *val = NULL;
    int len;
    if ((nfo = find_by_short_name(path)) == NULL) {
      // satisfy 'ls -l'
      /*
      struct stat sb;
      if (do_stat(path, &sb) != 0) return -ENOENT;
      */
      // just return 'no data' for any attribute of any path; it's harmful (i think)
      return 0;
    }
    if (strcmp(name, "user.realname") == 0) {
      val = get_str(nfo->realname);
    } else {
      for (size_t f = 0; f < ARRAYLEN(xattr_tags); ++f) {
        if (strcmp(name, xattr_tags[f].name) == 0) {
          val = get_str(get_tv(nfo->tags[xattr_tags[f].tflidx])->value);
          break;
        }
      }
    }
    if (val != NULL) {
      len = strlen(val);
      if (size > 0 && len > 0) memcpy(value, val, len);
      return len;
    }
    return 0;
  }
  //return -EPERM;
  return 0;
}


#define STRXCPY(str)  do { \
  const char *s = (str); \
  if (s != NULL && s[0]) { \
    int xlen = strlen(s)+1; \
    if (list != NULL) { memcpy(list, s, xlen); list += xlen; } \
    tlen += xlen; \
  } \
} while (0)


/** List extended attributes */
// size: the same note as for getxattr applies here.
// fill 'list' with asciiz-strings, one just after another.
// note that 'getfattr -d' lists only 'user.' attributes
static int tfs_listxattr (const char *path, char *list, size_t size) {
  fl_fileinfo_t *nfo;
  int tlen = 0;
  fd_call_dlogf("tfs_listxattr: path=[%s], list=0x%p, size=%d", path, list, size);
  if ((nfo = find_by_short_name(path)) == NULL) return -ENOENT;
  STRXCPY("user.realname");
  //
  for (size_t f = 0; f < ARRAYLEN(xattr_tags); ++f) if (!xattr_tags[f].hidden) STRXCPY(xattr_tags[f].name);
  //
  return tlen;
  //return -EPERM;
}


/** Remove extended attributes */
static int tfs_removexattr (const char *path, const char *name) {
  fd_call_dlogf("tfs_removexattr: path=[%s], name=[%s]", path, name);
  return -EPERM;
}


/** Open directory
 *
 * Unless the 'default_permissions' mount option is given,
 * this method should check if opendir is permitted for this
 * directory. Optionally opendir may also return an arbitrary
 * filehandle in the fuse_file_info structure, which will be
 * passed to readdir, closedir and fsyncdir.
 *
 * Introduced in version 2.3
 */
static int tfs_opendir (const char *path, struct fuse_file_info *fi) {
  dirinfo_t *odir;
  tagdb_check_and_reload();
  odir = list_dir(path);
  if (odir == NULL) {
    fd_call_dlogf("tfs_opendir: BAD [%s]\n", path);
    return -EPERM;
  }
  ++tfs_dirs_opened;
  fd_call_dlogf("tfs_opendir: path=[%s], %d entries", path, odir->fcount);
  fi->fh = (uintptr_t)odir; // fh is 64 bit, so this is safe
  return 0;
}


/** Read directory
 *
 * This supersedes the old getdir() interface.  New applications
 * should use this.
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.  This
 * works just like the old getdir() method.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 */
/*
 * typedef int (*fuse_fill_dir_t) (void *buf, const char *name, const struct stat *stbuf, off_t off);
 */
static int tfs_readdir (const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
  dirinfo_t *odir = (dirinfo_t *)(uintptr_t)fi->fh;
  struct stat stbuf, *starg = &stbuf;
  fd_call_dlogf("tfs_readdir: path=[%s], buf=0x%p, filler=0x%p, offset=%lld, fi=0x%p", path, buf, filler, offset, fi);
  if (odir == NULL) return -EPERM;
  //dlogf("tfs_readdir: path=[%s], %d entries", path, odir->fcount);
  if (offset < 0 || offset >= odir->fcount) return 0;
  // we can build 'struct stat' here, so readdir() will return some useful flags
  // but this is not really necessary, and we can just pass NULL
  if (do_stat(odir->items[offset].name, starg) != 0) starg = NULL; // don't pass anything on error
  //starg = NULL;
  if (filler(buf, odir->items[offset].name, starg, offset+1)) {
    fd_call_dlogf("tfs_readdir: path=[%s], filler failed!", path);
    return -ENOMEM;
  }
  //dlogf("tfs_readdir: path=[%s], name=[%s]", path, odir->items[offset].name);
  return 0;
}


/** Release directory
 *
 * Introduced in version 2.3
 */
static int tfs_releasedir (const char *path, struct fuse_file_info *fi) {
  dirinfo_t *odir = (dirinfo_t *)(uintptr_t)fi->fh;
  fd_call_dlogf("tfs_releasedir: path=[%s], fi=0x%p", path, fi);
  if (odir == NULL) {
    tagdb_check_and_reload();
    return -EPERM;
  }
  if (odir->items != NULL) free(odir->items);
  free(odir);
  fi->fh = 0;
  --tfs_dirs_opened;
  tagdb_check_and_reload();
  return 0;
}


/** Synchronize directory contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data
 *
 * Introduced in version 2.3
 */
// when exactly is this called?  when a user calls fsync and it
// happens to be a directory? ???
static int tfs_fsyncdir (const char *path, int datasync, struct fuse_file_info *fi) {
  fd_call_dlogf("tfs_fsyncdir: path=[%s], datasync=%d, fi=0x%p", path, datasync, fi);
  return 0;
}


/**
 * Initialize filesystem
 *
 * The return value will passed in the private_data field of
 * fuse_context to all file operations and as a parameter to the
 * destroy() method.
 *
 * Introduced in version 2.3
 * Changed in version 2.6
 */
// Undocumented but extraordinarily useful fact:  the fuse_context is
// set up before this function is called, and
// fuse_get_context()->private_data returns the user_data passed to
// fuse_main().  Really seems like either it should be a third
// parameter coming in here, or else the fact should be documented
// (and this might as well return void, as it did in older versions of
// FUSE).
static void *tfs_init (struct fuse_conn_info *conn) {
  char tmpbuf[4096];
  dlogf("tfs_init: protocol v%u.%u", conn->proto_major, conn->proto_minor);
  // dump caps
  if (conn->capable) {
    tmpbuf[0] = 0;
    if (conn->capable&FUSE_CAP_ASYNC_READ) strcat(tmpbuf, " FUSE_CAP_ASYNC_READ");
    if (conn->capable&FUSE_CAP_POSIX_LOCKS) strcat(tmpbuf, " FUSE_CAP_POSIX_LOCKS"); // filesystem supports "remote" locking
    if (conn->capable&FUSE_CAP_ATOMIC_O_TRUNC) strcat(tmpbuf, " FUSE_CAP_ATOMIC_O_TRUNC"); // filesystem handles the O_TRUNC open flag
    if (conn->capable&FUSE_CAP_EXPORT_SUPPORT) strcat(tmpbuf, " FUSE_CAP_EXPORT_SUPPORT"); // filesystem handles lookups of "." and ".."
    if (conn->capable&FUSE_CAP_BIG_WRITES) strcat(tmpbuf, " FUSE_CAP_BIG_WRITES"); // filesystem can handle write size larger than 4kB
    if (conn->capable&FUSE_CAP_DONT_MASK) strcat(tmpbuf, " FUSE_CAP_DONT_MASK"); // don't apply umask to file mode on create operations
    dlogf("kernel caps:%s\n", tmpbuf);
  }
  // set our params
  conn->want = 0; // the only flag we want is FUSE_CAP_BIG_WRITES, but we don't support writing
  conn->async_read = 0; // we don't support async reads
  return fuse_get_context()->private_data;
}


/**
 * Clean up filesystem
 *
 * Called on filesystem exit.
 *
 * Introduced in version 2.3
 */
static void tfs_destroy (void *userdata) {
  dlogf("tfs_destroy: userdata=0x%p", userdata);
}


/**
 * Check file access permissions
 *
 * This will be called for the access() system call.  If the
 * 'default_permissions' mount option is given, this method is not
 * called.
 *
 * This method is not called under Linux kernel versions 2.4.x
 *
 * Introduced in version 2.5
 */
//k8: don't know if mode can be combination of flags, and what to do then
static int tfs_access (const char *path, int mode) {
  struct stat st;
  int res;
  //dlogf("tfs_access: path=[%s], mode=0x%02x", path, mode);
  if (mode&W_OK) {
    //dlogf(" W_OK: [%s]\n", path);
    return -EPERM; // can't write
  }
  res = do_stat(path, &st);
  if (res == 0) {
    if (S_ISDIR(st.st_mode)) return 0;
    if (mode&X_OK) return -EPERM; // can't execute
    return 0;
  }
  return res;
}


/**
 * Create and open a file
 *
 * If the file does not exist, first create it with the specified
 * mode, and then open it.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the mknod() and open() methods
 * will be called instead.
 *
 * Introduced in version 2.5
 */
static int tfs_create (const char *path, mode_t mode, struct fuse_file_info *fi) {
  fd_call_dlogf("tfs_create: path=[%s], mode=0%03o, fi=0x%p", path, mode, fi);
  if (strcmp(path, "/:/reload") == 0) {
    // special action: reload tagdb
    filehandle_t *fh;
    tfs_need_reload = 1;
    // defer reloading, 'cause FUSE will call tfs_getattr() just after creating the eater
    //tagdb_check_and_reload();
    if ((fh = fht_create_eater(path)) != NULL) {
      fi->fh = (uintptr_t)fh;
      return 0;
    }
    return -EPERM;
  }
  //fi->fh = fd;
  return -EPERM;
}


/**
 * Change the size of an open file
 *
 * This method is called instead of the truncate() method if the
 * truncation was invoked from an ftruncate() system call.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the truncate() method will be
 * called instead.
 *
 * Introduced in version 2.5
 */
static int tfs_ftruncate (const char *path, off_t offset, struct fuse_file_info *fi) {
  //dlogf("tfs_ftruncate: path=[%s], offset=%lld, fi=0x%p", path, offset, fi);
  return -EPERM;
}


/**
 * Get attributes from an open file
 *
 * This method is called instead of the getattr() method if the
 * file information is available.
 *
 * Currently this is only called after the create() method if that
 * is implemented (see above).  Later it may be called for
 * invocations of fstat() too.
 *
 * Introduced in version 2.5
 */
static int tfs_fgetattr (const char *path, struct stat *statbuf, struct fuse_file_info *fi) {
  //dlogf("tfs_fgetattr: path=[%s], statbuf=0x%p, fi=0x%p", path, statbuf, fi);
  return tfs_getattr(path, statbuf); // just pass it up
}


/**
 * Perform POSIX file locking operation
 *
 * The cmd argument will be either F_GETLK, F_SETLK or F_SETLKW.
 *
 * For the meaning of fields in 'struct flock' see the man page
 * for fcntl(2).  The l_whence field will always be set to
 * SEEK_SET.
 *
 * For checking lock ownership, the 'fuse_file_info->owner'
 * argument must be used.
 *
 * For F_GETLK operation, the library will first check currently
 * held locks, and if a conflicting lock is found it will return
 * information without calling this method.	 This ensures, that
 * for local locks the l_pid field is correctly filled in.	The
 * results may not be accurate in case of race conditions and in
 * the presence of hard links, but it's unlikly that an
 * application would rely on accurate GETLK results in these
 * cases.  If a conflicting lock is not found, this method will be
 * called, and the filesystem may fill out l_pid by a meaningful
 * value, or it may leave this field zero.
 *
 * For F_SETLK and F_SETLKW the l_pid field will be set to the pid
 * of the process performing the locking operation.
 *
 * Note: if this method is not implemented, the kernel will still
 * allow file locking to work locally.  Hence it is only
 * interesting for network filesystems and similar.
 *
 * Introduced in version 2.6
 */
/*
static int tfs_lock (const char *path, struct fuse_file_info *fi, int cmd, struct flock *) {
  return -EPERM;
}
*/


/**
 * Change the access and modification times of a file with
 * nanosecond resolution
 *
 * Introduced in version 2.6
 */
static int tfs_utimens (const char *path, const struct timespec tv[2]) {
  return 0; // pretend that we done it
}


/**
 * Map block index within file to block index within device
 *
 * Note: This makes sense only for block device backed filesystems
 * mounted with the 'blkdev' option
 *
 * Introduced in version 2.6
 */
 /*
static int tfs_bmap (const char *path, size_t blocksize, uint64_t *idx) {
  return -EPERM;
}
*/


/**
 * Ioctl
 *
 * flags will have FUSE_IOCTL_COMPAT set for 32bit ioctls in
 * 64bit environment.  The size and direction of data is
 * determined by _IOC_*() decoding of cmd.  For _IOC_NONE,
 * data will be NULL, for _IOC_WRITE data is out area, for
 * _IOC_READ in area and if both are set in/out area.  In all
 * non-NULL cases, the area is of _IOC_SIZE(cmd) bytes.
 *
 * Introduced in version 2.8
 */
/*
static int tfs_ioctl (const char *path, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data) {
  return -EPERM;
}
*/


/**
 * Poll for IO readiness events
 *
 * Note: If ph is non-NULL, the client should notify
 * when IO readiness events occur by calling
 * fuse_notify_poll() with the specified ph.
 *
 * Regardless of the number of times poll with a non-NULL ph
 * is received, single notification is enough to clear all.
 * Notifying more times incurs overhead but doesn't harm
 * correctness.
 *
 * The callee is responsible for destroying ph with
 * fuse_pollhandle_destroy() when no longer in use.
 *
 * Introduced in version 2.8
 */
/*
static int tfs_poll (const char *path, struct fuse_file_info *fi, struct fuse_pollhandle *ph, unsigned *reventsp) {
  return -EPERM;
}
*/


////////////////////////////////////////////////////////////////////////////////
static const struct fuse_operations tfs_oper = {
  .getattr = tfs_getattr,
  .readlink = tfs_readlink,
  .getdir = NULL, // deprecated
  .mknod = tfs_mknod,
  .mkdir = tfs_mkdir,
  .unlink = tfs_unlink,
  .rmdir = tfs_rmdir,
  .symlink = tfs_symlink,
  .rename = tfs_rename,
  .link = tfs_link,
  .chmod = tfs_chmod,
  .chown = tfs_chown,
  .truncate = tfs_truncate,
  .utime = NULL, // deprecated
  .open = tfs_open,
  .read = tfs_read,
  .write = tfs_write,
  .statfs = tfs_statfs,
  .flush = tfs_flush,
  .release = tfs_release,
  .fsync = tfs_fsync,
  .setxattr = tfs_setxattr,
  .getxattr = tfs_getxattr,
  .listxattr = tfs_listxattr,
  .removexattr = tfs_removexattr,
  .opendir = tfs_opendir,
  .readdir = tfs_readdir,
  .releasedir = tfs_releasedir,
  .fsyncdir = tfs_fsyncdir,
  .init = tfs_init,
  .destroy = tfs_destroy,
  .access = tfs_access,
  .create = tfs_create,
  .ftruncate = tfs_ftruncate,
  .fgetattr = tfs_fgetattr,
  //.lock = tfs_lock,
  .utimens = tfs_utimens,
  //.bmap = tfs_bmap,
  //
  /**
   * Flag indicating, that the filesystem can accept a NULL path
   * as the first argument for the following operations:
   *
   * read, write, flush, release, fsync, readdir, releasedir,
   * fsyncdir, ftruncate, fgetattr and lock
   */
  //unsigned int flag_nullpath_ok : 1;
  .flag_nullpath_ok = 0,
  // 2.8 API
  //.ioctl = tfs_ioctl,
  //.poll = tfs_poll,
};


static void tfs_usage (void) {
  fprintf(stderr,
    "usage: fusedrv [mount options] tags.dat mountpoint\n"
    "special options:\n"
    "  --debug  write some debug logs\n"
    "  --files  show files not only in /:files\n"
  );
  exit(1);
}


static int tagfile_loaded = 0;
static const char *argv00 = "./me";


// return -1 on error, 0 if arg is to be discarded, 1 if arg should be kept
static int opt_processor (void *data, const char *arg, int key, struct fuse_args *outargs) {
  //fprintf(stderr, "key=%d; arg=%s\n", key, arg);
  //
  if (key == FUSE_OPT_KEY_OPT) {
    // this is not FUSE option, handle it
    // question: how to handle cases like '-x fuck'?
    if (strcmp(arg, "--debug") == 0) {
      char *me = strdup(argv00), *logname;
      if (strrchr(me, '/')) strrchr(me, '/')[0] = 0;
      logname = get_real_path(me, 1);
      free(me);
      if (logname != NULL) {
        char *ln = malloc(strlen(logname)+128);
        sprintf(ln, "%smuffin.log", logname);
        dbglog_set_filename(ln);
        dbglog_set_screenout(0);
        dbglog_set_fileout(1);
        free(ln);
        free(logname);
      }
      return 0;
    }
    //
    if (strcmp(arg, "--no-debug") == 0) {
      dbglog_set_screenout(0);
      dbglog_set_fileout(0);
      return 0;
    }
    //
    if (strcmp(arg, "--debug-calls") == 0) {
      opt_log_calls = 1;
      return 0;
    }
    //
    if (strcmp(arg, "--files") == 0) {
      opt_files_only_in_files = 0;
      return 0;
    }
    //
    if (strcmp(arg, "--no-files") == 0) {
      opt_files_only_in_files = 1;
      return 0;
    }
    //
    if (strcmp(arg, "--help") == 0) {
      tfs_usage();
      return 0;
    }
    //
    if (strcmp(arg, "-h") == 0) { tfs_usage(); exit(0); }
    //fprintf(stderr, "FATAL: unknown option '%s'!\n", arg);
    //exit(1);
    return 1;
  }
  //
  if (key == FUSE_OPT_KEY_NONOPT) {
    // not an option; first non-option arg is tagdb file name
    if (!tagfile_loaded) {
      uint64_t stt, ste;
      tagfilename = get_real_path(arg, 0);
      if (tagfilename == NULL) { fprintf(stderr, "FATAL: can't determine tag file name!\n"); return -1; }
      printf("loading database...\n");
      stt = k8clock();
      if (tagdb_load(tagfilename) != 0) { fprintf(stderr, "FATAL: can't load tagfile: '%s'!\n", tagfilename); return -1; }
      ste = k8clock();
      printf("database load time: %.15g seconds\n", (double)(ste-stt)/1000.0);
      printf("building hashes...\n");
      stt = k8clock();
      build_hashes();
      ste = k8clock();
      printf("hash build time: %.15g seconds\n", (double)(ste-stt)/1000.0);
      //if (file_info_count == 0) { fprintf(stderr, "FATAL: no valid files in tagfile: '%s'!\n", arg); exit(1); }
      tagfile_loaded = 1;
      return 0;
    }
    //
    return 1;
  }
  //
  return 1;
}


int main (int argc, char *argv[]) {
  int fuse_stat;
  struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
  //
  if (argc > 0 && argv[0] != NULL) argv00 = argv[0];
  //
  dbglog_set_screenout(0);
  dbglog_set_fileout(0);
  //
  if (getuid() == 0 || geteuid() == 0) {
    fprintf(stderr, "FATAL: running fusedrv as root is inappropriate!\n");
    return 1;
  }
  //dlogf("=== starting fusedrv ===");
  //
  fuse_opt_parse(&args, NULL, NULL, opt_processor);
  if (!tagfile_loaded) tfs_usage();
  //fuse_opt_add_arg(&args, "-omodules=subdir,subdir=/foo");
  fuse_opt_add_arg(&args, "-s"); // single-threaded
  //
  //for (int f = 0; f < args.argc; ++f) fprintf(stderr, "arg#%d: [%s]\n", f, args.argv[f]);
  //
  //fprintf(stderr, "about to call fuse_main\n");
  fuse_stat = fuse_main(args.argc, args.argv, &tfs_oper, (void *)666);
  //fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
  //
  clear_hashes();
  tagdb_unload();
  fuse_opt_free_args(&args);
  return fuse_stat;
}
