/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "translit.h"

#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


static const struct {
  uint8_t code;
  const char *ch;
} atrans[74] = {
  {0xE1, "A"}, //�
  {0xE0, "YU"}, //�
  {0xE3, "C"}, //�
  {0xE2, "B"}, //�
  {0xE5, "E"}, //�
  {0xE4, "D"}, //�
  {0xE7, "G"}, //�
  {0xE6, "F"}, //�
  {0xE9, "I"}, //�
  {0xE8, "H"}, //�
  {0xEB, "K"}, //�
  {0xEA, "IJ"}, //�
  {0xED, "M"}, //�
  {0xEC, "L"}, //�
  {0xEF, "O"}, //�
  {0xEE, "N"}, //�
  {0xF1, "YA"}, //�
  {0xF0, "P"}, //�
  {0xF3, "S"}, //�
  {0xF2, "R"}, //�
  {0xF5, "U"}, //�
  {0xF4, "T"}, //�
  {0xF7, "V"}, //�
  {0xF6, "J"}, //�
  {0xF9, "Y"}, //�
  {0xF8, ""}, //�
  {0xFB, "SH"}, //�
  {0xFA, "Z"}, //�
  {0xFD, "SCH"}, //�
  {0xFC, "E"}, //�
  {0xFF, ""}, //�
  {0xFE, "CH"}, //�
  {0xA3, "yo"}, //�
  {0xA4, "ye"}, //�
  {0xA7, "yj"}, //�
  {0xA6, "i"}, //�
  {0xAD, "g"}, //�
  {0xB3, "YO"}, //�
  {0xB4, "YE"}, //�
  {0xB7, "YJ"}, //�
  {0xB6, "I"}, //�
  {0xBF, "(c)"}, //�
  {0xC1, "a"}, //�
  {0xC0, "yu"}, //�
  {0xC3, "c"}, //�
  {0xC2, "b"}, //�
  {0xC5, "e"}, //�
  {0xC4, "d"}, //�
  {0xC7, "g"}, //�
  {0xC6, "f"}, //�
  {0xC9, "i"}, //�
  {0xC8, "h"}, //�
  {0xCB, "k"}, //�
  {0xCA, "ij"}, //�
  {0xCD, "m"}, //�
  {0xCC, "l"}, //�
  {0xCF, "o"}, //�
  {0xCE, "n"}, //�
  {0xD1, "ya"}, //�
  {0xD0, "p"}, //�
  {0xD3, "s"}, //�
  {0xD2, "r"}, //�
  {0xD5, "u"}, //�
  {0xD4, "t"}, //�
  {0xD7, "v"}, //�
  {0xD6, "j"}, //�
  {0xD9, "y"}, //�
  {0xD8, ""}, //�
  {0xDB, "sh"}, //�
  {0xDA, "z"}, //�
  {0xDD, "sch"}, //�
  {0xDC, "e"}, //�
  {0xDF, ""}, //�
  {0xDE, "ch"}, //�
};


static inline const char *findTRC (char c) {
  uint8_t cc = c&0xff;
  for (size_t f = 0; f < sizeof(atrans)/sizeof(atrans[0]); ++f) {
    if (atrans[f].code == cc) return atrans[f].ch;
  }
  return NULL;
}


char *translitstr (const char *src) {
  char *res, *dest;
  if (src == NULL) src = "";
  res = dest = calloc(strlen(src)*4+2, sizeof(char));
  for (int f = 0; f < strlen(src); ++f) {
    const char *ch = findTRC(src[f]);
    if (ch != NULL) {
      for (; *ch; ++ch) *dest++ = le2lower(*ch);
    } else {
      *dest++ = le2lower(src[f]);
    }
  }
  *dest = '\0';
  return res;
}
