/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
////////////////////////////////////////////////////////////////////////////////
// augment UTHASH with two more macrodefs
#define HASH_FIND_PTR_EX(hh,head,findptr,out)  HASH_FIND(hh,head,findptr,sizeof(void *),out)
#define HASH_ADD_PTR_EX(hh,head,ptrfield,add)  HASH_ADD(hh,head,ptrfield,sizeof(void *),add)

#define HASH_FIND_STR_EX(hh,head,findstr,out)  HASH_FIND(hh,head,findstr,strlen(findstr),out)
#define HASH_ADD_STR_EX(hh,head,strfield,add)  HASH_ADD(hh,head,strfield,strlen(add->strfield),add)

#define HASH_FIND_UINT32(head,findint,out)              HASH_FIND(hh,head,findint,sizeof(uint32_t),out)
#define HASH_ADD_UINT32(head,intfield,add)              HASH_ADD(hh,head,intfield,sizeof(uint32_t),add)
#define HASH_REPLACE_UINT32(head,intfield,add,replaced) HASH_REPLACE(hh,head,intfield,sizeof(uint32_t),add,replaced)


////////////////////////////////////////////////////////////////////////////////
enum {
  TFL_YEAR,
  TFL_ARTIST_O,
  TFL_ARTIST_T,
  TFL_ALBUM_O,
  TFL_ALBUM_T,
  TFL_TITLE_O,
  TFL_TITLE_T,
  TFL_GENRE_O,
  TFL_GENRE_T,
  TFL_YRAL_O,
  TFL_YRAL_T,
  TFL_MAX
};


typedef struct __attribute__((packed,gcc_struct)) {
  uint32_t count;
  uint32_t idx;
} fl_tfl_t;

typedef struct __attribute__((packed,gcc_struct)) {
  uint32_t value;
  fl_tfl_t tfl[TFL_MAX];
} fl_tagvalue_t;


typedef struct __attribute__((packed,gcc_struct)) {
  uint32_t realname;
  uint32_t shortname;
#ifdef USE_DEVID_INODE
  uint64_t devid;
  uint64_t inode;
#endif
  uint64_t size;
  uint64_t mtime;
  uint8_t tracknum;
  uint16_t year;
  union {
    struct {
      uint32_t year_tag;
      uint32_t artist_o, artist_t;
      uint32_t album_o, album_t;
      uint32_t title_o, title_t;
      uint32_t genre_o, genre_t;
      uint32_t yral_o, yral_t;
    };
    uint32_t tags[TFL_MAX];
  };
} fl_fileinfo_t;


////////////////////////////////////////////////////////////////////////////////
static uint32_t file_count = 0, tag_count = 0, string_bytes = 0, o_tag_count = 0, t_tag_count = 0, tfl_bytes = 0;
static uint8_t *string_data = NULL;
static fl_fileinfo_t *fileinfo_data = NULL;
static fl_tagvalue_t *tagvalue_data = NULL;
static uint32_t *taglisto_data = NULL;
static uint32_t *taglistt_data = NULL;
static uint32_t *tfl_data = NULL;

static inline fl_tagvalue_t *get_tv (uint32_t idx) { return (idx < tag_count ? &tagvalue_data[idx] : NULL); }
static inline fl_fileinfo_t *get_fi (uint32_t idx) { return (idx < file_count ? &fileinfo_data[idx] : NULL); }

static inline uint32_t tv2idx (const fl_tagvalue_t *tv) { return (tv != NULL ? (uintptr_t)((const uint8_t *)tv-(const uint8_t *)tagvalue_data)/sizeof(*tv) : 0); }
static inline uint32_t fi2idx (const fl_fileinfo_t *fi) { return (fi != NULL ? (uintptr_t)((const uint8_t *)fi-(const uint8_t *)fileinfo_data)/sizeof(*fi) : 0); }

static inline char *get_str (uint32_t ofs) { return (ofs >= 2 && ofs < string_bytes ? (char *)(string_data+ofs) : NULL); }
static inline int get_strlen (uint32_t ofs) { return (ofs < string_bytes ? *((const uint16_t *)(string_data+ofs-2)) : 0); }


////////////////////////////////////////////////////////////////////////////////
static void tagdb_unload (void) {
  file_count = tag_count = string_bytes = o_tag_count = t_tag_count = tfl_bytes = 0;
  if (string_data != NULL) free(string_data);
  if (fileinfo_data != NULL) free(fileinfo_data);
  if (tagvalue_data != NULL) free(tagvalue_data);
  if (taglisto_data != NULL) free(taglisto_data);
  if (taglistt_data != NULL) free(taglistt_data);
  if (tfl_data != NULL) free(tfl_data);
  //
  string_data = NULL;
  fileinfo_data = NULL;
  tagvalue_data = NULL;
  taglisto_data = NULL;
  taglistt_data = NULL;
  tfl_data = NULL;
}


////////////////////////////////////////////////////////////////////////////////
static int tagdb_load (const char *fname) {
  FILE *fl;
  char sign[4];
  //
#if defined(NDEBUG) && NDEBUG
# define showofs()  ((void)0)
#else
  void showofs (void) {
    dlogf("OFS: 0x%08x\n", (unsigned)ftell(fl));
  }
#endif
  //
  tagdb_unload();
  fl = fopen(fname, "r");
  if (fl == NULL) return -1; // no tags
  // header
  if (fread(sign, 4, 1, fl) != 1) goto error;
#ifdef USE_DEVID_INODE
  if (memcmp(sign, "MTD0", 4) != 0) goto error;
#else
  if (memcmp(sign, "MTD2", 4) != 0) goto error;
#endif
  if (fread(&file_count, 4, 1, fl) != 1) goto error;
  if (fread(&tag_count, 4, 1, fl) != 1) goto error;
  if (fread(&string_bytes, 4, 1, fl) != 1) goto error;
  if (fread(&tfl_bytes, 4, 1, fl) != 1) goto error;
  if (fread(&o_tag_count, 4, 1, fl) != 1) goto error;
  if (fread(&t_tag_count, 4, 1, fl) != 1) goto error;
  //
  //dlogf("%u\n", sizeof(fl_tagvalue_t));
  dlogf("file_count=%u\n", file_count);
  dlogf("tag_count=%u\n", tag_count);
  dlogf("string_bytes=%u\n", string_bytes);
  dlogf("tfl_bytes=%u\n", tfl_bytes);
  dlogf("o_tag_count=%u\n", o_tag_count);
  dlogf("t_tag_count=%u\n", t_tag_count);
  //
  if (file_count < 1 || tag_count < 1 || string_bytes < 3 || tfl_bytes < 1 || o_tag_count < 1 || t_tag_count < 1) goto error; // invalid or empty file
  //
  printf("loading %u files...\n", (unsigned int)file_count);
  dlogf("loading strings...\n");
  showofs();
  if ((string_data = malloc(string_bytes)) == NULL) goto error;
  if (fread(string_data, string_bytes, 1, fl) != 1) goto error;
  //
  dlogf("loading taglisto...\n");
  showofs();
  if ((taglisto_data = malloc(o_tag_count*sizeof(uint32_t))) == NULL) goto error;
  if (fread(taglisto_data, o_tag_count*sizeof(uint32_t), 1, fl) != 1) goto error;
  //
  dlogf("loading taglistt...\n");
  showofs();
  if ((taglistt_data = malloc(t_tag_count*sizeof(uint32_t))) == NULL) goto error;
  if (fread(taglistt_data, t_tag_count*sizeof(uint32_t), 1, fl) != 1) goto error;
  //
  dlogf("loading fileinfo...\n");
  showofs();
  if ((fileinfo_data = malloc(file_count*sizeof(fl_fileinfo_t))) == NULL) goto error;
  if (fread(fileinfo_data, file_count*sizeof(fl_fileinfo_t), 1, fl) != 1) goto error;
  //
  dlogf("loading taginfo...\n");
  showofs();
  if ((tagvalue_data = malloc(tag_count*sizeof(fl_tagvalue_t))) == NULL) goto error;
  if (fread(tagvalue_data, tag_count*sizeof(fl_tagvalue_t), 1, fl) != 1) goto error;
  //
  dlogf("loading tfl...\n");
  showofs();
  if ((tfl_data = malloc(tfl_bytes)) == NULL) goto error;
  if (fread(tfl_data, tfl_bytes, 1, fl) != 1) goto error;
  //
  dlogf("done\n");
  fclose(fl);
  //
  return 0;
error:
  // invalid tag file
  fclose(fl);
  tagdb_unload();
  return -1;
}
