/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* debug logs */
#include "dbglog.h"

#define DBG_TMP_BUF_SIZE  (65536)

#ifdef WIN32
# include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/types.h>


////////////////////////////////////////////////////////////////////////////////
DbgLogUpdateFn dbgLogPreUpdate = NULL;
DbgLogUpdateFn dbgLogPostUpdate = NULL;


////////////////////////////////////////////////////////////////////////////////
static char ccLock = 0;


static void dbgLock (void) {
  while (__sync_bool_compare_and_swap(&ccLock, 0, 1)) {
#ifndef WIN32
    usleep(1);
#else
    Sleep(1); // i don't care
#endif
  }
}


static void dbgUnlock (void) {
  //__sync_synchronize();
  //ccLock = 0;
  __sync_fetch_and_and(&ccLock, 0);
}


////////////////////////////////////////////////////////////////////////////////
#ifndef NO_DEBUG_LOG
static char dbgLogFileName[8192] = {0};
static int dbgLogWriteToFile = 1;
static int dbgLogWriteToScr = 0;
#endif


#ifndef NO_DEBUG_LOG
static __attribute__((constructor(DBGLOG_CONSTRUCTOR_PRIO))) void dbgLogInit (void) {
  if (!dbgLogFileName[0]) dbglog_set_filename("dlog.log");
}
#endif


void dbglog_set_filename (const char *fname) {
#ifndef NO_DEBUG_LOG
  dbgLock();
  if (fname && fname[0]) {
    strcpy(dbgLogFileName, fname);
  } else {
    dbgLogFileName[0] = '\0';
  }
  dbgUnlock();
#endif
}


int dbglog_set_fileout (int doIt) {
#ifndef NO_DEBUG_LOG
  int old;
  dbgLock();
  old = dbgLogWriteToFile;
  dbgLogWriteToFile = (doIt ? 1 : 0);
  dbgUnlock();
  return old;
#else
  return 0;
#endif
}


int dbglog_set_screenout (int doIt) {
#ifndef NO_DEBUG_LOG
  int old;
  dbgLock();
  old = dbgLogWriteToScr;
  dbgLogWriteToScr = (doIt ? 1 : 0);
  dbgUnlock();
  return old;
#else
  return 0;
#endif
}


#ifndef NO_DEBUG_LOG
__attribute__((format(printf, 1, 2))) void dlogf (const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  dlogfVA(fmt, ap);
  va_end(ap);
}


static void writebuf (FILE *fo, const char *sbuf, const char *pfx) {
  int plen = (pfx != NULL ? strlen(pfx) : 0);
  if (!sbuf[0]) {
    if (plen > 0) fwrite(pfx, plen, 1, fo);
    fwrite("\n", 1, 1, fo);
    return;
  }
  while (*sbuf) {
    const char *nlp;
    int olen;
    if (plen > 0) fwrite(pfx, plen, 1, fo);
    nlp = strchr(sbuf, '\n');
    if (nlp == NULL) {
      olen = strlen(sbuf);
      nlp = sbuf+olen;
    } else {
      olen = nlp-sbuf;
      if (olen > 1 && nlp[-1] == '\r') --olen;
      ++nlp; // skip 'newline'
    }
    if (olen > 0) fwrite(sbuf, olen, 1, fo);
    fwrite("\n", 1, 1, fo);
    sbuf = nlp;
  }
}


void dlogfVA (const char *fmt, va_list inap) {
  static char timebuf[128];
  static char buf[DBG_TMP_BUF_SIZE];
  time_t t;
  struct tm *bt;
  if (fmt == NULL && !fmt[0]) return;
  dbgLock();
  if (dbgLogWriteToFile || dbgLogWriteToScr) {
    vsnprintf(buf, sizeof(buf), fmt, inap);
    if (dbgLogPreUpdate) dbgLogPreUpdate();
    t = time(NULL);
    bt = localtime(&t);
    snprintf(timebuf, sizeof(timebuf), "[%05u] %04d/%02d/%02d %02d:%02d:%02d: ",
      (unsigned int)getpid(),
      bt->tm_year+1900, bt->tm_mon+1, bt->tm_mday,
      bt->tm_hour, bt->tm_min, bt->tm_sec
    );
    if (dbgLogWriteToFile && dbgLogFileName[0]) {
      FILE *outfile = fopen(dbgLogFileName, "ab");
      if (outfile) {
        writebuf(outfile, buf, timebuf);
        fclose(outfile);
      }
    }
    if (dbgLogWriteToScr && stderr != NULL) writebuf(stderr, buf, timebuf+13); // skip PID and year
    if (dbgLogPostUpdate) dbgLogPostUpdate();
  }
  dbgUnlock();
}
#endif
