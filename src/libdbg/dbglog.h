/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* debug logs */
#ifndef _DBGLOG_H_
#define _DBGLOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>

/*#define NO_DEBUG_LOG*/


typedef void (*DbgLogUpdateFn) (void);
extern DbgLogUpdateFn dbgLogPostUpdate;
extern DbgLogUpdateFn dbgLogPreUpdate;


#define DBGLOG_CONSTRUCTOR_PRIO  (555)


/* defaults: write to file; don't write to stderr */

extern void dbglog_set_filename (const char *fname);
extern int dbglog_set_screenout (int doIt);
extern int dbglog_set_fileout (int doIt);
#ifndef NO_DEBUG_LOG
extern void dlogf (const char *fmt, ...) __attribute__((format(printf, 1, 2)));
extern void dlogfVA (const char *fmt, va_list ap);
#else
# define dlogf(fmt,...)          (void(0))
# define dlogfVA(fmt,ap)         (void(0))
#endif


#ifdef __cplusplus
}
#endif
#endif
