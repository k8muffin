/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

//#include <ctype.h>
#include <dirent.h>
#include <iconv.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <tag_c.h>

#include "pbar.h"

#include "common.h"

#include "libdbg/dbglog.h"
#include "translit.h"
#include "uthash.h"
#include "libk8clock/k8clock.h"


////////////////////////////////////////////////////////////////////////////////
#include "tagload.c"


////////////////////////////////////////////////////////////////////////////////
#define UNDEF_ARTIST_O  "unknown artist"
#define UNDEF_ALBUM_O   "unknown album"
#define UNDEF_TITLE_O   "unknown title"
#define UNDEF_GENRE_O   "unknown genre"

#define UNDEF_ARTIST_T  "unknown_artist"
#define UNDEF_ALBUM_T   "unknown_album"
#define UNDEF_TITLE_T   "unknown_title"
#define UNDEF_GENRE_T   "unknown_genre"

#define UNDEF_YEAR  "unknown_year"


////////////////////////////////////////////////////////////////////////////////
static int opt_verbose = 0;


////////////////////////////////////////////////////////////////////////////////
static const unsigned char utf8Length[256] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x00-0x0f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x10-0x1f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x20-0x2f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x30-0x3f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x40-0x4f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x50-0x5f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x60-0x6f
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x70-0x7f
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0x80-0x8f
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0x90-0x9f
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0xa0-0xaf
  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, //0xb0-0xbf
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, //0xc0-0xcf  c0-c1: overlong encoding: start of a 2-byte sequence, but code point <= 127
  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, //0xd0-0xdf
  3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3, //0xe0-0xef
  4,4,4,4,4,8,8,8,8,8,8,8,8,8,8,8  //0xf0-0xff
};


static inline int is_possible_utf8 (const void *buf) {
  for (const unsigned char *data = (const unsigned char *)buf; *data; ++data) if (*data >= 128) return 1;
  return 0;
}


static inline int is_valid_utf8 (const void *buf) {
  const unsigned char *data = (const unsigned char *)buf;
  //fprintf(stderr, "UTF: [%s]\n", (const char *)buf);
  while (*data) {
    uint32_t uc;
    unsigned char len = utf8Length[*data];
    //fprintf(stderr, "0x%02x: %u\n", *data, len);
    switch (len) {
      case 0: // ascii
        ++data;
        continue;
      case 8: case 9: // invalid
        //fprintf(stderr, "BAD0: 0x%02x\n", *data);
        return 0;
    }
    // utf-8
    uc = (*data++)&(0x7c>>len);
    while (--len) {
      if (!data[0]) {
        //fprintf(stderr, "BADEOF: len=%u\n", len+1);
        return 0;
      }
      if (utf8Length[*data] != 9) {
        //fprintf(stderr, "BAD1: 0x%02x\n", *data);
        return 0;
      }
      uc = (uc<<6)|((*data++)&0x3f);
    }
    if (uc > 0x10ffff) return 0;
    if ((uc >= 0xd800 && uc <= 0xdfff) || // utf16/utf32 surrogates
        (uc >= 0xfdd0 && uc <= 0xfdef) || // just for fun
        (uc >= 0xfffe && uc <= 0xffff)) {
      // bad unicode
      //fprintf(stderr, "BAD2: 0x%04x\n", uc);
      return 0;
    }
  }
  return 1;
}


static inline int notutf (const char *str) {
  if (str != NULL) {
    int havehi = 0;
    for (const unsigned char *s = (const unsigned char *)str; *s; ++s) if (*s >= 128) { havehi = 1; break; }
    if (havehi) {
      //printf("[%s]: %d\n", str, is_valid_utf8(str));
      return (is_valid_utf8(str) == 0);
    }
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static void trimstr (char *s) {
  if (s != NULL && s[0]) {
    //char *olds = strdup(s), *xs = s;
    char *ns, *p;
    // change all '/' to underlines
    for (ns = s; *ns; ++ns) if (*ns == '/' || *ns == 127) *ns = '_';
    // remove duplicate underlines
    for (ns = p = s; *ns; ++ns) {
      if (*ns == '_') {
        if (p > s && p[-1] != '_') *p++ = *ns;
      } else {
        *p++ = *ns;
      }
    }
    *p = 0;
    // remove duplicate spaces
    for (ns = p = s; *ns; ++ns) {
      if (leIsSpace(*ns)) {
        if (p > s && !leIsSpace(p[-1])) *p++ = *ns;
      } else {
        *p++ = *ns;
      }
    }
    *p = 0;
    // trim trailing spaces and underlines
    ns = s+strlen(s)-1;
    while (ns >= s && (leIsSpace(*ns) || *ns == '_')) --ns;
    ns[1] = 0;
    // trim leading spaces and underlines
    for (ns = s; *ns && (leIsSpace(*ns) || *ns == '_'); ++ns) ;
    if (ns != s) memmove(s, ns, strlen(ns)+1);
    //
    //fprintf(stderr, "trans: [%s] -> [%s]\n", olds, xs); free(olds);
  }
}


////////////////////////////////////////////////////////////////////////////////
//static int opt_koi8 = 1; // not yet


#define xalloca(ptr,msize)  do { \
  size_t msz = (msize); \
  (ptr) = alloca(msz); \
  memset((ptr), 0, msz); \
} while (0)


static char *str_transliterate (const char *str, int inkoi) {
  iconv_t cd;
  size_t il, ol, ool;
  char *outs, *ibuf, *obuf, *res;
  int asis = 1;
  //fprintf(stderr, "str_transliterate: \"%s\" (%d)\n", str, is_valid_utf8(str));
  if (str == NULL) return strdup("");
  //if (!str[0]) { outs = strdup(str); goto done; }
  for (const unsigned char *u = (const unsigned char *)str; *u; ++u) if (*u >= 128) { asis = 0; break; }
  if (asis) { outs = strdup(str); goto done; }
  //
  if (!inkoi) {
    cd = iconv_open("koi8-u//translit//ignore", "utf-8");
    if (cd == (iconv_t)-1) {
      fprintf(stderr, "FATAL: can't create conversion object!\n");
      abort();
      return NULL;
    }
    xalloca(outs, strlen(str)*6+4);
    ibuf = (char *)str;
    obuf = outs;
    il = strlen(str);
    ool = ol = il*4;
    il = iconv(cd, &ibuf, &il, &obuf, &ol);
    iconv_close(cd);
    if (il == (size_t)-1) {
      fprintf(stderr, "CONVERSION FUCKED: [%s]\n", str);
      abort();
      return NULL;
    }
    xalloca(res, ool-ol+1);
    if (ool-ol > 0) memcpy(res, outs, ool-ol);
    //free(outs);
    if (ool-ol == 0) {
      //FUCK!
      fprintf(stderr, "FUCKFUCK: [%s]\n", str);
    }
    outs = translitstr(res);
    //free(res);
    //fprintf(stderr, "STT: [%s] -> [%s]\n", res, outs);
  } else {
    outs = translitstr(str);
    //fprintf(stderr, "STT: [%s] -> [%s]\n", str, outs);
  }
done:
  for (char *s = outs; *s; ++s) {
    *s = le2lower(*s);
    if (!leIsAlNum(*s)) *s = '_';
  }
  trimstr(outs);
  /*
  while (*outs && *outs == '_') memmove(outs, outs+1, strlen(outs));
  if (outs[0]) {
    obuf = outs+1;
    for (char *s = outs+1; *s; ++s) {
      if (*s == '_') {
        if (obuf[-1] != '_') *obuf++ = *s;
      } else {
        *obuf++ = *s;
      }
    }
    while (obuf > outs && obuf[-1] == '_') --obuf;
    *obuf = 0;
  }
  */
  return outs;
}


////////////////////////////////////////////////////////////////////////////////
static char *str_tokoi (const char *str) {
  iconv_t cd;
  size_t il, ol, ool;
  char *outs, *ibuf, *obuf, *res;
  int asis = 1;
  if (str == NULL) return strdup("");
  if (str == NULL || !str[0]) { res = strdup(str); goto done; }
  for (const unsigned char *u = (const unsigned char *)str; *u; ++u) if (*u >= 128) { asis = 0; break; }
  if (asis) { res = strdup(str); goto done; }
  cd = iconv_open("koi8-u//translit//ignore", "utf-8");
  if (cd == (iconv_t)-1) return NULL;
  outs = calloc(1, strlen(str)*6+4);
  if (outs == NULL) {
    iconv_close(cd);
    return NULL;
  }
  ibuf = (char *)str;
  obuf = outs;
  il = strlen(str);
  ool = ol = il*4;
  il = iconv(cd, &ibuf, &il, &obuf, &ol);
  iconv_close(cd);
  if (il == (size_t)-1) {
    free(outs);
    return NULL;
  }
  res = calloc(ool-ol+1, 1);
  if (ool-ol > 0) memcpy(res, outs, ool-ol);
  free(outs);
done:
  return res;
}


// do some analyzis here
//  1. convert 'c' if it is surrounded by russian letters
//  2. convert 'c' if it is the only english letter in string (but there can be other 'c')
// 's' must be lowercased
static void normalize_letter_c (unsigned char *s) {
  int have_rus = 0; // have some russian letters except 'c'?
  int have_eng = 0; // have some english letters except 'c'?
  int have_rus_c = 0; // have russian 'c'?
  int have_eng_c = 0; // have english 'c'?
  for (const unsigned char *t = s; *t; ++t) {
    if (*t == 'c') { have_eng_c = 1; continue; }
    if (*t == '�') { have_rus_c = 1; continue; }
    if (*t >= 128) { have_rus = 1; continue; }
    if (*t >= 'a' && *t <= 'z') { have_eng = 1; continue; }
  }
  //
  if (!have_rus_c && !have_eng_c) return; // nothing to do
  if (!have_eng_c && !have_eng) return; // nothing to do
  if (!have_rus_c && !have_rus) return; // nothing to do
  // process string
  for (unsigned char *t = s; *t; ++t) {
    if (*t == 'c') {
      // english
      if (!have_eng || t[1] >= 128 || (t > s && t[-1] >= 128)) {
        // easy case, convert
        *t = '�';
        continue;
      }
      // not surrounded by russian letters, let it be
    } else if (*t == '�') {
      // russian
      if (!have_rus || t[1] < 128 || (t > s && t[-1] < 128)) {
        // easy case, convert
        *t = 'c';
        continue;
      }
      // not surrounded by english letters, let it be
    }
  }
}


// string must be converted to koi8
// caller must free() result
//TODO: maybe use some kind of soundex here?
static char *normalize_tag_str (const char *s, const char *def) {
  char *res, *d, *t;
  d = t = alloca(strlen(s)+1);
  // trim various shit, convert other shit
  for (; *s; ++s) {
    unsigned char ch = (unsigned char)(*s);
    if (leIsSpace(*s)) continue;
    if (ch == 127) continue;
    *d = le2lower(*s);
    if (ch >= 128) {
      if (*d == '�') *d = '�';
      if (*d == '�') *d = '�';
      //if (*d == '�') *d = 'o'; // rus->lat
      //if (*d == '�') *d = 'a'; // rus->lat
      ++d;
    } else if (leIsDigit(*d) || (*d >= 'a' && *d <= 'z')) {
      ++d;
    }
  }
  *d = 0;
  normalize_letter_c((unsigned char *)t);
  if (t[0]) {
    // now transliterate
    res = translitstr(t);
  } else {
    //fprintf(stderr, "! [%s] [%s] [%s]\n", s, t, def);
    //res = strdup(def);
    if (def[0]) return normalize_tag_str(def, "");
    return strdup(def);
  }
  // and trim: just in case
  trimstr(res);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
static char *w2u (const char *str, int justcheck) {
  iconv_t cd;
  size_t il, ol, ool;
  char *outs, *ibuf, *obuf, *res;
  int asis = 1;
  if (str == NULL) return (justcheck ? "" : strdup(""));
  if (str == NULL || !str[0]) return (justcheck ? (char *)str : strdup(str));
  for (const unsigned char *u = (const unsigned char *)str; *u; ++u) if (*u >= 128) { asis = 0; break; }
  if (asis) return (justcheck ? (char *)str : strdup(str));
  cd = iconv_open("utf-8", "cp1251");
  if (cd == (iconv_t)-1) return NULL;
  outs = malloc(strlen(str)*6+4);
  if (outs == NULL) {
    iconv_close(cd);
    return NULL;
  }
  ibuf = (char *)str;
  obuf = outs;
  il = strlen(str);
  ool = ol = il*4;
  il = iconv(cd, &ibuf, &il, &obuf, &ol);
  iconv_close(cd);
  if (il == (size_t)-1) {
    free(outs);
    return NULL;
  }
  res = calloc(ool-ol+1, 1);
  if (ool-ol > 0) memcpy(res, outs, ool-ol);
  free(outs);
  if (justcheck) free(res);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
//k8 hack
static char *fix_venya (char *s) {
  int again;
  //if (strstr(s, "venya") == NULL) return s;
  //fprintf(stderr, "VVV: [%s]\n", s);
wow_again:
  again = 0;
  //
  for (;;) {
    char *t = strstr(s, "d_rkin");
    if (t == NULL) break;
    //fprintf(stderr, "V0: [%s] -> [", s);
    memmove(t+1, t+2, strlen(t+2)+1);
    //fprintf(stderr, "%s]\n", s);
    again = 1;
  }
  if (again) goto wow_again;
  //
  for (;;) {
    char *t = strstr(s, "dr_kin");
    if (t == NULL) break;
    //fprintf(stderr, "V0: [%s] -> [", s);
    memmove(t+2, t+3, strlen(t+3)+1);
    //fprintf(stderr, "%s]\n", s);
    again = 1;
  }
  if (again) goto wow_again;
  //
  for (;;) {
    char *t = strstr(s, "venya_drkin");
    if (t == NULL) break;
    //fprintf(stderr, "V1: [%s] -> [", s);
    memmove(t, t+6, strlen(t+6)+1);
    //fprintf(stderr, "%s]\n", s);
    again = 1;
  }
  if (again) goto wow_again;
  //
  return s;
}


static char *str_convert (const char *str, const char *def, const char *fname, int toutf) {
  const char *ostr = str;
  char *ss, *s;
  static char res[8192];
  if (str == NULL || !str[0]) {
    if (def == NULL) return NULL; // skip this tag
    ostr = str = def;
  }
  if (toutf) {
    char *t = w2u(str, 0), *t1;
    if (t == NULL) {
      dlogf("FUCK! %s: [%s]\n", fname, str);
      abort();
    }
    t1 = str_tokoi(t);
    free(t);
    //printf("u: [%s] -> [%s]\n", str, t);
    ostr = str = t1;
  } else {
    // utf8 -> koi
    char *t = str_tokoi(str);
    ostr = str = t;
  }
  s = ss = alloca(strlen(str)+1);
  strcpy(s, str);
  trimstr(s);
  //if (strcmp(s, ostr) != 0) dlogf("%s: [%s] -> [%s]\n", fname, ostr, s);
  //fprintf(stderr, "strconvert: [%s] -> [%s] (%d) (%d)\n", ostr, s, is_valid_utf8(ostr), is_valid_utf8(s));
  //if (toutf) free((void *)ostr);
  free((void *)ostr);
  memset(res, 0, sizeof(res));
  if (!s[0]) {
    //fprintf(stderr, " EMPTY! [def=%s]\n", def);
    if (def != NULL) {
      strncpy(res, def, sizeof(res)-1);
      return res;
    }
    return NULL;
  }
  strncpy(res, s, sizeof(res)-1);
  //
  return fix_venya(res);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  uint16_t len;
  char *str;
  uint32_t ofs; // used on saving
  UT_hash_handle hh;
} string_t;

static string_t *strings = NULL; // hash
//static char *stringdata = NULL;
static int stringdata_used = 0;
//static int stringdata_alloted = 0;


#define xabort()  do { uint8_t *p = (uint8_t *)0; *p = 0; } while (0)

// len<0: use strlen()
static string_t *string_find_add (const char *str, int len, int allow_add) {
  string_t *s;
  if (str == NULL) { fprintf(stderr, "FATAL: string_find_add(): null string!\n"); xabort(); }
  if (len < 0) len = strlen(str);
  if (len == 0) { fprintf(stderr, "FATAL: empty string in string_find()!\n"); xabort(); }
  if (len > 65535) { fprintf(stderr, "FATAL: string too long in string_find()!\n"); abort(); }
  HASH_FIND(hh, strings, str, len, s);
  if (s == NULL && allow_add) {
    //uint16_t l16 = len;
    s = calloc(1, sizeof(*s));
    if (s == NULL) { fprintf(stderr, "FATAL: out of memory in string_find()!\n"); abort(); }
    /*
    if (stringdata_used+len+1+2 > stringdata_alloted) {
      int newsz = ((stringdata_used+len+1+2)|0x000fffff)+1; // grow by 1MB
      char *n = realloc(stringdata, newsz);
      if (n == NULL) { fprintf(stderr, "FATAL: out of memory in string_find()!\n"); abort(); }
      fprintf(stderr, "string_find_add: realloced from %d to %d\n", stringdata_alloted, newsz);
      stringdata_alloted = newsz;
      stringdata = n;
    }
    */
    s->len = len;
    // store length
    //memcpy(stringdata+stringdata_used, &l16, 2);
    stringdata_used += 2; // length
    // store string
    //s->str = stringdata+stringdata_used;
    s->str = calloc(len+1, 1);
    s->ofs = stringdata_used;
    if (len > 0) memcpy(s->str, str, len);
    stringdata_used += len;
    ++stringdata_used; // trailing zero
    //stringdata[stringdata_used++] = 0;
    HASH_ADD_KEYPTR(hh, strings, s->str, len, s);
  }
  return s;
}


static inline string_t *string_find (const char *str, int len) { return string_find_add(str, len, 0); }
static inline string_t *cstring_find (const char *str) { return string_find_add(str, -1, 0); }
static inline string_t *string_add (const char *str, int len) { return string_find_add(str, len, 1); }
static inline string_t *cstring_add (const char *str) { return string_find_add(str, -1, 1); }


////////////////////////////////////////////////////////////////////////////////
typedef struct fileinfo_t fileinfo_t;
typedef struct mtagvalue_t mtagvalue_t;


typedef struct {
  uint32_t count;
  uint32_t alloted;
  uint32_t idx; // increases monotonically
  fileinfo_t **files;
} tfl_t;


struct mtagvalue_t {
  string_t *value;
  tfl_t tfl[TFL_MAX];
  uint32_t idx; // increases monotonically
  char *nval; // 'normalized' value (as normalize_tag_str() do)
  UT_hash_handle hh; // by 'value'
  UT_hash_handle hh_nv; // by 'nval'
  UT_hash_handle hh_o; // by 'value', in 'tagolist'
  UT_hash_handle hh_t; // by 'value', in 'tagtlist'
};


typedef struct {
  uint64_t dev;
  uint64_t node;
} devnode_t;


struct fileinfo_t {
  devnode_t di;
  string_t *realname;
  string_t *shortname;
  uint64_t size;
  uint64_t mtime;
  uint8_t tracknum;
  uint16_t year;
  union {
    struct {
      mtagvalue_t *year_tag;
      mtagvalue_t *artist_o, *artist_t;
      mtagvalue_t *album_o, *album_t;
      mtagvalue_t *title_o, *title_t;
      mtagvalue_t *genre_o, *genre_t;
      mtagvalue_t *yral_o, *yral_t;
    };
    mtagvalue_t *tags[TFL_MAX];
  };
  uint32_t fidx; // increases monotonically
  char *snamestr; // duplicate for hashing
  UT_hash_handle hh; // by 'realname'
  UT_hash_handle hh_inode; // by 'di'
  UT_hash_handle hh_sname; // by 'shortname' (used exclusively in postprocessor)
};

static mtagvalue_t *taglist = NULL; // hash
static mtagvalue_t *tagolist = NULL; // hash
static mtagvalue_t *tagtlist = NULL; // hash
static mtagvalue_t *taglist_nv = NULL; // hash
static uint32_t tflbytes_used = 0; // set by postprocessor


// string should be trimmed and so on
// set 'do_norm' to 0 if this is 'transliterated' tag value
// str is UTF-8
static mtagvalue_t *tag_add (const char *str, const char *def, string_t *oval, int dontreg) {
  mtagvalue_t *tv, *tvx;
  string_t *value;
  int addns = 0, dofree = 0;
  if (str == NULL || !str[0]) { fprintf(stderr, "FATAL: empty string in tag_add()!\n"); abort(); }
  //if (!is_valid_utf8(str)) { fprintf(stderr, "[%s] %d\n", str, is_valid_utf8(str)); abort(); }
  if (oval != NULL) {
    // try to find normalized version
    //TODO: hash
    char *nn = normalize_tag_str(oval->str, def);
    if (!nn[0]) {
      fprintf(stderr, "XFUCK! o: [%s] [%s]\n", oval->str, nn);
      xabort();
    }
    HASH_FIND_STR_EX(hh_nv, taglist_nv, nn, tvx);
    if (tvx != NULL) {
      //fprintf(stderr, "oval hit: [%s] -> [%s] ([%s])\n", str, nn, tvx->value->str);
      // found one, change
      str = tvx->value->str;
    } else {
      addns = 1;
    }
    free(nn);
    /*
    char *nn = normalize_tag_str(oval->str);
    for (tvx = taglist; tvx != NULL; tvx = tvx->hh.next) if (strcmp(tvx->nval, nn) == 0) break;
    if (tvx != NULL) str = tvx->value->str; // found one, change
    free(nn);
    */
    //fprintf(stderr, "tag_trans: [%s]\n", str);
    // and transliterate anyway
    //fprintf(stderr, "tag_add: [%s]\n", str);
    //const char *ostr = str;
    str = str_transliterate(str, 1);
    if (!str[0]) {
      //fprintf(stderr, "FUCK! o: [%s];  [%s] -> [%s] [%s]\n", oval->str, ostr, str, def);
      //xabort();
      free((char *)str);
      str = str_transliterate(def, 1);
      //fprintf(stderr, "FUCK! o: [%s];  [%s] -> [%s] [%s]\n", oval->str, ostr, str, def);
      //xabort();
    }
    dofree = 1;
  }
  if (str[0] == ':') {
    char *s = strdup(str);
    fprintf(stderr, "FUCK! o: [%s]; [%s] [%s]\n", (oval != NULL ? oval->str : "*"), str, def);
    if (s[1]) {
      s[0] = '_';
    } else {
      free(s);
      s = strdup("_");
    }
    //xabort();
    if (dofree) free((char *)str);
    str = s;
    dofree = 1;
  } else if (str[0] == '.') {
    char *s = malloc(strlen(str)+4);
    sprintf(s, "_%s", str);
    if (dofree) free((char *)str);
    str = s;
    dofree = 1;
  }
  if (!dofree) {
    str = strdup(str);
    dofree = 1;
  }
  fix_venya((char *)str);
  value = cstring_add(str);
  HASH_FIND_PTR(taglist, &value, tv);
  if (tv == NULL) {
    tv = calloc(1, sizeof(*tv));
    if (tv == NULL) { fprintf(stderr, "FATAL: out of memory in tag_add()\n"); abort(); }
    tv->value = value;
    //TODO: check memory error!
    tv->nval = normalize_tag_str(value->str, def); // this will be used in transliterated tag search
    if (addns) HASH_ADD_KEYPTR(hh_nv, taglist_nv, tv->nval, strlen(tv->nval), tv);
    HASH_ADD_PTR(taglist, value, tv);
  }
  //
  if (dontreg == 0 || dontreg == 666) {
    if (dontreg == 666 || oval == NULL) {
      // original
      HASH_FIND_PTR_EX(hh_o, tagolist, &value, tvx);
      if (tvx == NULL) HASH_ADD_PTR_EX(hh_o, tagolist, value, tv);
    }
    if (dontreg == 666 || oval != NULL) {
      // transliterated
      HASH_FIND_PTR_EX(hh_t, tagtlist, &value, tvx);
      if (tvx == NULL) HASH_ADD_PTR_EX(hh_t, tagtlist, value, tv);
    }
  }
  //
  if (dofree) free((char *)str);
  return tv;
}


static void tag_add_fi (mtagvalue_t *tv, fileinfo_t *fi, int tflidx) {
  tfl_t *tfl = &tv->tfl[tflidx];
  for (int f = 0; f < tfl->count; ++f) if (tfl->files[f] == fi) return;
  if (tfl->count+1 > tfl->alloted) {
    int newsz = ((tfl->count+1)|0xff)+1;
    fileinfo_t **n = realloc(tfl->files, sizeof(tfl->files[0])*newsz);
    if (n == NULL) { fprintf(stderr, "FATAL: out of memory in tag_add_fi()\n"); abort(); }
    tfl->alloted = newsz;
    tfl->files = n;
  }
  tfl->files[tfl->count++] = fi;
}


static void tags_postprocess (void) {
  //uint32_t cnt = 0;
  uint32_t idx = 0;
  tflbytes_used = 0;
  for (mtagvalue_t *tv = taglist; tv != NULL; tv = tv->hh.next) {
    //if (tv->count == 0) { fprintf(stderr, "WARNING: unused tag value: '%s'!\n", tv->value->str); }
    tv->idx = idx++;
    //if ((uint64_t)ofs+size >= 0x0fffffffUL) { fprintf(stderr, "FATAL: tags_postprocess(): we are too many!\n"); abort(); }
    for (int f = 0; f < TFL_MAX; ++f) {
      tfl_t *tfl = &tv->tfl[f];
      uint32_t sz = sizeof(uint32_t)*tfl->count;
      tfl->idx = tflbytes_used/sizeof(uint32_t);
      tflbytes_used += sz;
      // sort 'files', 'cause fileinfo_t 'idx' SHOULD be ok here
      qsort(tfl->files, tfl->count, sizeof(tfl->files[0]), lambda(int, (const void *p0, const void *p1) {
        const fileinfo_t *i0 = *((const fileinfo_t **)p0);
        const fileinfo_t *i1 = *((const fileinfo_t **)p1);
        return (i0->fidx < i1->fidx ? -1 : (i0->fidx > i1->fidx ? 1 : 0));
      }));
    }
    //++cnt;
  }
  //fprintf(stderr, "cnt=%u; count=%d; bytes=%u\n", cnt, HASH_COUNT(taglist), tagbytes_used);
}


////////////////////////////////////////////////////////////////////////////////
static fileinfo_t *filelist = NULL; // hash by file names
//static fileinfo_t *filelist_ino = NULL; // hash by inodes
//static uint32_t cur_fileinfo_idx = 0;


/*
static fileinfo_t *fileinfo_find_inode (dev_t dd, ino_t inode) {
  if (inode != 0) {
    fileinfo_t *fi;
    devnode_t di;
    di.dev = dd;
    di.node = inode;
    HASH_FIND(hh_inode, filelist_ino, &di, sizeof(di), fi);
    return fi;
  }
  return NULL;
}


static void fileinfo_add_inode (fileinfo_t *fi) {
  if (fi->di.node != 0) {
    fileinfo_t *xf = fileinfo_find_inode(fi->di.dev, fi->di.node);
    if (xf == NULL) {
      HASH_ADD_KEYPTR(hh_inode, filelist_ino, &fi->di, sizeof(fi->di), fi);
    }
  }
}
*/


// 'realname' should be full pathname!
// don't forget to update filelist_ino!
static fileinfo_t *fileinfo_find (const char *realname) {
  fileinfo_t *fi;
  string_t *value;
  if (realname == NULL || !realname[0]) { fprintf(stderr, "FATAL: empty path in fileinfo_find()!\n"); abort(); }
  if (realname[0] != '/' || realname[strlen(realname)-1] == '/') { fprintf(stderr, "FATAL: invalid path in fileinfo_find(): '%s'\n", realname); abort(); }
  value = cstring_find(realname);
  if (value == NULL) return NULL;
  HASH_FIND_PTR(filelist, &value, fi);
  return fi;
}


// 'realname' should be full pathname!
// don't forget to update filelist_ino!
static fileinfo_t *fileinfo_add (const char *realname) {
  fileinfo_t *fi;
  string_t *value;
  if (realname == NULL || !realname[0]) { fprintf(stderr, "FATAL: empty path in fileinfo_add()!\n"); abort(); }
  if (realname[0] != '/' || realname[strlen(realname)-1] == '/') { fprintf(stderr, "FATAL: invalid path in fileinfo_add(): '%s'\n", realname); abort(); }
  value = cstring_add(realname);
  HASH_FIND_PTR(filelist, &value, fi);
  if (fi == NULL) {
    fi = calloc(1, sizeof(*fi));
    if (fi == NULL) { fprintf(stderr, "FATAL: out of memory in fileinfo_add()\n"); abort(); }
    fi->realname = value;
    //fi->fidx = cur_fileinfo_idx++;
    //if ((uint64_t)cur_fileinfo_ofs+sizeof(fl_fileinfo_t) >= 0x0fffffffUL) { fprintf(stderr, "FATAL: fileinfo_add(): we are too many!\n"); abort(); }
    HASH_ADD_PTR(filelist, realname, fi);
  } else {
    fprintf(stderr, "DUPADD: [%s] [%s]\n", realname, fi->realname->str);
    abort();
  }
  return fi;
}


// only postprocessor can build shortnames!
//TODO: prepend track number to short name if there is no one?
//TODO: process files without extensions, but with dots in file name.
//      very low priority: just don't do that!
//TODO: generate short name with tags (skipping 'unknown title')
static void fileinfo_postprocess (void) {
  fileinfo_t *snhash = NULL;
  uint32_t cur_fileinfo_idx = 0;
  for (fileinfo_t *fi = filelist; fi != NULL; fi = fi->hh.next) {
    //fileinfo_t *oldfi;
    string_t *snv;
    fi->fidx = cur_fileinfo_idx++;
    if (fi->shortname == NULL) {
      // generate short name
      const char *onamep = strrchr(fi->realname->str, '/')+1; // this CAN'T fail!
      const char *oext = strrchr(onamep, '.');
      char *trs, *tmp = NULL;
      if (oext == onamep) { fprintf(stderr, "strangely named file: '%s'\n", fi->realname->str); }
      if (strcmp(fi->title_t->value->str, UNDEF_TITLE_T) != 0) {
        // generate from title
        //fprintf(stderr, "t: [%s]\n", fi->title_t->value->str);
        tmp = malloc(strlen(fi->title_t->value->str)+(oext != NULL ? strlen(oext) : 0)+32);
        if (fi->tracknum == 0) {
          int trno = 0;
          if (leIsDigit(onamep[0])) {
            trno = onamep[0]-'0';
            if (leIsDigit(onamep[1])) {
              trno = trno*10+onamep[1]-'0';
              if (leIsDigit(onamep[2])) {
                trno = 0;
              }
            }
          }
          if (trno == 0) {
            sprintf(tmp, "%s%s", fi->title_t->value->str, (oext != NULL ? oext : ""));
          } else {
            sprintf(tmp, "%02d_%s%s", trno, fi->title_t->value->str, (oext != NULL ? oext : ""));
          }
        } else {
          sprintf(tmp, "%02u_%s%s", fi->tracknum, fi->title_t->value->str, (oext != NULL ? oext : ""));
        }
        //fprintf(stderr, "generated: [%s] : [%s]\n", onamep, tmp);
        // in koi
      } else {
        // transliterate utf-8 or koi
        //fprintf(stderr, "dt: [%s]\n", fi->title_t->value->str);
        if (is_possible_utf8(onamep)) {
          if (is_valid_utf8(onamep)) {
            // convert to koi-8
            tmp = str_tokoi(onamep);
            if (tmp == NULL) { fprintf(stderr, "FATAL: fileinfo_postprocess() failed to convert encoding!\n"); abort(); }
          }
        }
        if (tmp == NULL) tmp = strdup(onamep);
      }
      // normalize
      if (oext != NULL) strrchr(tmp, '.')[0] = 0;
      if (!tmp[0]) { fprintf(stderr, "VERY strangely named file: '%s'\n", fi->realname->str); }
      trs = str_transliterate(tmp, 1);
      trimstr(trs);
      if (oext != NULL) {
        //fprintf(stderr, "tmp=%p; trs=%p; oext=%p\n", tmp, trs, oext);
        tmp = realloc(tmp, strlen(trs)+strlen(oext)+4);
        sprintf(tmp, "%s%s", trs, oext);
        for (char *s = tmp+strlen(trs); *s; ++s) *s = le2lower(*s);
        free(trs);
        trs = tmp;
      }
      /*
      // fuck all nonalnums, convert to lowercase
      for (unsigned char *s = (unsigned char *)trs; *s; ++s) {
        if (*s == 127 || (*s < 128 || !leIsAlNum(*s))) *s = '_';
        *s = le2lower(*s);
      }
      */
      // trim and normalize name
      //trimstr(trs);
      // add extension back
      //if (oext != NULL) strcat(trs, oext);
      // add track number if any
      if (fi->tracknum > 0) {
        int tno = 0;
        if (leIsDigit(*trs)) {
          // starts with digit, parse number
          for (const char *t = trs; *t && leIsDigit(*t); ++t) {
            tno = tno*10+t[0]-'0';
            if (tno > 255 || t-trs > 2) break; // alas, this is definitely not a track number
          }
        }
        if (tno != fi->tracknum) {
          // there is no track number, let's add it
          char *nn = malloc(strlen(trs)+8);
          sprintf(nn, "%02u_%s", fi->tracknum, trs);
          free(trs);
          trs = nn;
        }
      }
      //
      if (trs[0] == '.') {
        char *n = malloc(strlen(trs)+4);
        sprintf(n, "_%s", trs);
        free(trs);
        trs = n;
      }
      //
      if ((snv = cstring_find(trs)) != NULL) {
        // this name can be is taken, check it better and generate new one
        fileinfo_t *xf;
        // first check if this name is really taken
        HASH_FIND_STR_EX(hh_sname, snhash, trs, xf);
        if (xf != NULL) {
          // yes, the name is really taken, let's generate new one
          char *newname = malloc(strlen(trs)+(oext != NULL ? strlen(oext) : 0)+16);
          char *ext = strrchr(trs, '.'); // all dots except the last one are annihilated here
          int cnt;
          if (ext != NULL) *ext = 0; // remove extension
          for (cnt = 1; cnt < 1000000; ++cnt) {
            sprintf(newname, "%s_%d%s", trs, cnt, (oext != NULL ? oext : ""));
            HASH_FIND_STR_EX(hh_sname, snhash, newname, xf);
            if (xf == NULL) break; // found free name
          }
          if (cnt >= 1000000) { fprintf(stderr, "FATAL: too many files with the same name in fileinfo_postprocess()!\n"); abort(); }
          //fprintf(stderr, "renamed: [%s%s] -> [%s]\n", trs, (oext != NULL ? oext : ""), newname);
          free(trs);
          trs = newname;
          snv = cstring_add(trs);
        }
        // if the name is not taken, snv is correct here
      } else {
        // this name is surely free, use it
        snv = cstring_add(trs);
      }
      // set short name
      fi->shortname = snv;
      fi->snamestr = snv->str;
    } else {
      // we already have short name for this file, wow!
      // heh, do nothing except sanity check
      fileinfo_t *xf;
      //
      snv = fi->shortname;
      HASH_FIND_STR_EX(hh_sname, snhash, snv->str, xf);
      if (xf != NULL) { fprintf(stderr, "FATAL: duplicate preserved short name!\n"); abort(); }
    }
    HASH_ADD_KEYPTR(hh_sname, snhash, fi->snamestr, strlen(fi->snamestr), fi);
    //fprintf(stderr, "[%s] -> [%s]\n", onamep, fi->shortname->str);
  }
  // now delete hash, we don't need it anymore
  HASH_CLEAR(hh_sname, snhash);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  char *name;
  devnode_t di;
  UT_hash_handle hh; // either name
  UT_hash_handle hh_di;
} dirlist_t;


static dirlist_t *direxcept_name = NULL;
static dirlist_t *direxcept_inode = NULL;


static dirlist_t *direxcept_find (const char *name, dev_t dd, ino_t inode) {
  if (inode != 0) {
    dirlist_t *d;
    devnode_t di;
    di.dev = dd;
    di.node = inode;
    HASH_FIND(hh_di, direxcept_inode, &di, sizeof(di), d);
    if (d != NULL) return d;
  }
  if (name != NULL && name[0]) {
    dirlist_t *d;
    HASH_FIND_STR(direxcept_name, name, d);
    return d;
  }
  return NULL;
}


static void direxcept_add (const char *name, dev_t dd, ino_t inode) {
  dirlist_t *d = direxcept_find(name, dd, inode);
  if (d != NULL) return;
  d = calloc(1, sizeof(*d));
  if (name != NULL && name[0]) {
    static char ppbuf[PATH_MAX+1];
    char *pp = realpath(name, ppbuf);
    if (pp == NULL || pp[0] != '/') {
      fprintf(stderr, "FATAL: realpath(\"%s\") fucked! [%s]\n", name, pp);
      abort();
    }
    if (pp[strlen(pp)-1] != '/') strcat(pp, "/");
    d->name = strdup(pp);
  }
  d->di.dev = dd;
  d->di.node = inode;
  if (inode != 0) {
    dirlist_t *dl;
    devnode_t di;
    di.dev = dd;
    di.node = inode;
    HASH_FIND(hh_di, direxcept_inode, &di, sizeof(di), dl);
    if (dl == NULL) HASH_ADD_KEYPTR(hh_di, direxcept_inode, &d->di, sizeof(d->di), d);
  }
  if (d->name != NULL) {
    dirlist_t *dl;
    HASH_FIND_STR(direxcept_name, d->name, dl);
    if (dl == NULL) HASH_ADD_KEYPTR(hh, direxcept_name, d->name, strlen(d->name), d);
  }
}


////////////////////////////////////////////////////////////////////////////////
static int fill_tags (const char *filename) {
  int res = 0;
  TagLib_Tag *tag;
  //const TagLib_AudioProperties *properties;
  TagLib_File *file;
  const char *ext = strrchr(filename, '.');
  int dotest;
  struct stat st;
  fileinfo_t *fi;
  if (ext == NULL || (strcasecmp(ext, ".mp3") == 0)) {
    taglib_set_strings_unicode(0);
    dotest = 1;
  } else {
    taglib_set_strings_unicode(1);
    dotest = 0;
  }
  //TODO: check inode!
  if (fileinfo_find(filename) != NULL) return 0; // already done
  if (stat(filename, &st) != 0) { fprintf(stderr, "NO FILE: [%s]\n", filename); return 0; }
  if (!S_ISREG(st.st_mode)) { fprintf(stderr, "NOT A REGULAR FILE: [%s]\n", filename); return 0; }
  //
  file = taglib_file_new(filename);
  if (file == NULL) return 0;
  tag = taglib_file_tag(file);
  if (tag != NULL) {
    int toutf;
    const char *artist = taglib_tag_artist(tag);
    const char *album = taglib_tag_album(tag);
    const char *title = taglib_tag_title(tag);
    const char *genre = taglib_tag_genre(tag);
    unsigned int yy = taglib_tag_year(tag);
    unsigned int tno = taglib_tag_track(tag);
    //
    if (!dotest) goto doutf8;
    if (notutf(artist) ||
        notutf(album) ||
        notutf(title) ||
        notutf(genre)) {
      if (w2u(artist, 1) == NULL) goto doutf8;
      if (w2u(album, 1) == NULL) goto doutf8;
      if (w2u(title, 1) == NULL) goto doutf8;
      if (w2u(genre, 1) == NULL) goto doutf8;
      toutf = 1;
    } else {
doutf8:
      taglib_tag_free_strings();
      taglib_file_free(file);
      //
      taglib_set_strings_unicode(1);
      file = taglib_file_new(filename);
      if (file == NULL) return 0;
      tag = taglib_file_tag(file);
      if (tag == NULL) {
        taglib_tag_free_strings();
        taglib_file_free(file);
        return 0;
      }
      toutf = 0;
      artist = taglib_tag_artist(tag);
      album = taglib_tag_album(tag);
      title = taglib_tag_title(tag);
      genre = taglib_tag_genre(tag);
      yy = taglib_tag_year(tag);
      tno = taglib_tag_track(tag);
    }
    //
    fi = fileinfo_add(filename);
    //memset(fi, 0, sizeof(*fi));
    //TODO: check for memory errors!
    //fi->filename = strdup(filename);
    fi->di.dev = st.st_dev;
    fi->di.node = st.st_ino;
    fi->size = st.st_size;
    fi->mtime = st.st_mtime;
    //fileinfo_add_inode(fi);
    //
    fi->artist_o = tag_add(str_convert(artist, UNDEF_ARTIST_O, filename, toutf), UNDEF_ARTIST_O, NULL, 0);
    fi->album_o = tag_add(str_convert(album, UNDEF_ALBUM_O, filename, toutf), UNDEF_ALBUM_O, NULL, 0);
    fi->title_o = tag_add(str_convert(title, UNDEF_TITLE_O, filename, toutf), UNDEF_TITLE_O, NULL, 0);
    fi->genre_o = tag_add(str_convert(genre, UNDEF_GENRE_O, filename, toutf), UNDEF_GENRE_O, NULL, 1);
    fi->artist_t = tag_add(str_convert(artist, UNDEF_ARTIST_T, filename, toutf), UNDEF_ARTIST_T, fi->artist_o->value, 0);
    fi->album_t = tag_add(str_convert(album, UNDEF_ALBUM_T, filename, toutf), UNDEF_ALBUM_T, fi->album_o->value, 0);
    fi->title_t = tag_add(str_convert(title, UNDEF_TITLE_T, filename, toutf), UNDEF_TITLE_T, fi->title_o->value, 0);
    fi->genre_t = tag_add(str_convert(genre, UNDEF_GENRE_T, filename, toutf), UNDEF_GENRE_T, fi->genre_o->value, 1);
    //
    tag_add_fi(fi->artist_o, fi, TFL_ARTIST_O);
    tag_add_fi(fi->album_o, fi, TFL_ALBUM_O);
    tag_add_fi(fi->title_o, fi, TFL_TITLE_O);
    tag_add_fi(fi->genre_o, fi, TFL_GENRE_O);
    tag_add_fi(fi->artist_t, fi, TFL_ARTIST_T);
    tag_add_fi(fi->album_t, fi, TFL_ALBUM_T);
    tag_add_fi(fi->title_t, fi, TFL_TITLE_T);
    tag_add_fi(fi->genre_t, fi, TFL_GENRE_T);
    //
    fi->year = yy;
    // sanitize year
    if (fi->year > 0) {
      if (fi->year < 30) fi->year += 2000;
      else if (fi->year < 100) fi->year += 1900;
      else if (fi->year < 1930) fi->year = 0;
    }
    if (fi->year > 0) {
      char buf[32];
      sprintf(buf, "%u", fi->year);
      fi->year_tag = tag_add(buf, UNDEF_YEAR, NULL, 666);
    } else {
      fi->year_tag = tag_add(UNDEF_YEAR, UNDEF_YEAR, NULL, 666);
    }
    tag_add_fi(fi->year_tag, fi, TFL_YEAR);
    //
    {
      char *ss = alloca(strlen(fi->year_tag->value->str)+strlen(fi->album_t->value->str)+strlen(fi->album_o->value->str)+16);
      if (fi->year > 0) {
        sprintf(ss, "%s %s", fi->year_tag->value->str, fi->album_o->value->str);
        fi->yral_o = tag_add(ss, UNDEF_ALBUM_O, NULL, 0);
        sprintf(ss, "%s_%s", fi->year_tag->value->str, fi->album_t->value->str);
        fi->yral_t = tag_add(ss, UNDEF_ALBUM_T, fi->yral_o->value, 0);
      } else {
        sprintf(ss, "_%s", fi->album_o->value->str);
        fi->yral_o = tag_add(ss, UNDEF_ALBUM_O, NULL, 0);
        sprintf(ss, "_%s", fi->album_t->value->str);
        fi->yral_t = tag_add(ss, UNDEF_ALBUM_T, fi->yral_o->value, 0);
      }
      tag_add_fi(fi->yral_o, fi, TFL_YRAL_O);
      tag_add_fi(fi->yral_t, fi, TFL_YRAL_T);
    }
    //
    fi->tracknum = (tno < 256 ? tno : 0);
    //
    if (fi->tracknum == 0) {
      // try to determine track number from file name
      const char *name = strrchr(filename, '/');
      if (name == NULL) name = filename; else ++name;
      if (leIsDigit(name[0]) && leIsDigit(name[1]) && name[2] == '_') {
        // ok, it's my format for track numbers
        fi->tracknum = (name[0]-'0')*10+name[1]-'0';
      }
    }
    // fuck comments
    res = 1;
  }
  //
  /*
  properties = taglib_file_audioproperties(file);
  if (properties != NULL) {
    int seconds = taglib_audioproperties_length(properties)%60;
    int minutes = (taglib_audioproperties_length(properties)-seconds)/60;
    printf("-- AUDIO --\n");
    printf("bitrate    : %d\n", taglib_audioproperties_bitrate(properties));
    printf("sample rate: %d\n", taglib_audioproperties_samplerate(properties));
    printf("channels   : %d\n", taglib_audioproperties_channels(properties));
    printf("length     : %d:%02i\n", minutes, seconds);
  }
  */
  //
  taglib_tag_free_strings();
  taglib_file_free(file);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
// 0: normal file; 1: dir; <0: error
/*
static inline int getftype (const char *fname) {
  struct stat st;
  if (stat(fname, &st) == 0) {
    if (S_ISDIR(st.st_mode)) return 1;
    if (S_ISREG(st.st_mode)) return 0;
  }
  return -1;
}
*/


static void process_dir (const char *path) {
  struct stat st;
  int ignore = 0;
  { //HACK!
    static const char *ifiles[] = {
      ".mufignore",
      ".muffinignore",
      ".muf_ignore",
      ".muffin_ignore",
    };
    char *nd = malloc(strlen(path)+64), *nn;
    strcpy(nd, path);
    if (nd[strlen(nd)-1] != '/') strcat(nd, "/");
    nn = nd+strlen(nd);
    for (size_t f = 0; f < sizeof(ifiles)/sizeof(ifiles[0]); ++f) {
      strcpy(nn, ifiles[f]);
      if (access(nd, F_OK) == 0) { ignore = 1; break; }
    }
    free(nd);
  }
  if (ignore) {
    if (opt_verbose > 1) printf("skipping: [%s]\n", path);
    return;
  }
  if (opt_verbose > 1) printf("trying: [%s]\n", path);
  if (stat(path, &st) == 0 && S_ISDIR(st.st_mode)) {
    DIR *dp = opendir(path);
    if (dp != NULL) {
      struct dirent *de;
      char *rd, *fn;
      int message_shown = 0;
      {
        static char ppbuf[PATH_MAX+1];
        char *pp = realpath(path, ppbuf);
        if (pp == NULL || pp[0] != '/') {
          fprintf(stderr, "FATAL: realpath(\"%s\") fucked! [%s]\n", path, pp);
          abort();
        }
        //rd = malloc(strlen(pp)+4);
        rd = alloca(strlen(pp)+4);
        strcpy(rd, pp);
        if (rd[strlen(rd)-1] != '/') strcat(rd, "/");
        if (direxcept_find(rd, st.st_dev, st.st_ino)) {
          printf("***SKIP: [%s]\n", rd);
          closedir(dp);
          return;
        }
        // add this dir to 'exception list', so we will not loop
        direxcept_add(rd, st.st_dev, st.st_ino);
        //
        //fn = malloc(strlen(rd)+8192);
        fn = alloca(strlen(rd)+257);
      }
      //printf("scanning: [%s]\n", rd);
      while ((de = readdir(dp)) != NULL) {
        struct stat st;
        if (de->d_name[0] == '.' && (!de->d_name[1] || (de->d_name[1] == '.' && !de->d_name[2]))) continue;
        if (!de->d_name[0]) continue; // just in case
        sprintf(fn, "%s%s", rd, de->d_name);
         if (opt_verbose > 1) printf(" checking: %s\n", fn);
        if (stat(fn, &st) != 0) continue;
        if (S_ISDIR(st.st_mode)) {
          //strcat(fn, "/");
          //fprintf(stderr, "[%s]\n", fn);
          process_dir(fn);
        } else if (S_ISREG(st.st_mode)) {
#ifdef USE_DEVID_INODE
          fileinfo_t *fi;
          if ((fi = fileinfo_find_inode(st.st_dev, st.st_ino)) == NULL) {
            int res = fill_tags(fn);
            if (res < 0) { fprintf(stderr, "FATAL: writing error!\n"); abort(); }
            if (res > 0) {
              if (!message_shown) {
                printf("scanning: [%s]\n", rd);
                message_shown = 1;
              }
            }
          } else {
            //fprintf(stderr, "duplicate: [%s] : [%s]\n", fn, fi->realname->str);
          }
#else
          if (opt_verbose) {
            if (!message_shown) { printf("scanning: [%s]\n", rd); message_shown = 1; }
            write(1, " ", 1);
            write(1, fn, strlen(fn));
            write(1, " ... ", 5);
          }
          int res = fill_tags(fn);
          if (res < 0) { fprintf(stderr, "FATAL: writing error!\n"); abort(); }
          if (opt_verbose) write(1, "OK\n", 3);
          if (res > 0) {
            if (!message_shown) { printf("scanning: [%s]\n", rd); message_shown = 1; }
          }
#endif
        }
      }
      //free(fn);
      //free(rd);
    }
    closedir(dp);
  }
}


////////////////////////////////////////////////////////////////////////////////
static int write_tagfile (const char *filename) {
  FILE *fo;
  uint32_t u32;
  fo = fopen(filename, "w");
  if (fo == NULL) return -1;
  // signature
#ifdef USE_DEVID_INODE
  if (fwrite("MTD0", 4, 1, fo) != 1) goto error;
#else
  if (fwrite("MTD2", 4, 1, fo) != 1) goto error;
#endif
  // header
  printf("writing header...\n");
  u32 = HASH_COUNT(filelist);
  if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  u32 = HASH_COUNT(taglist);
  if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  u32 = stringdata_used;
  if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  u32 = tflbytes_used;
  if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  u32 = HASH_CNT(hh_o, tagolist);
  if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  u32 = HASH_CNT(hh_t, tagtlist);
  if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  // strings
  printf("writing strings...\n");
  long fpos = ftell(fo);
  for (const string_t *s = strings; s != NULL; s = s->hh.next) {
    uint16_t len = s->len;
    if (fwrite(&len, 2, 1, fo) != 1) goto error;
    if (fwrite(s->str, s->len+1, 1, fo) != 1) goto error;
  }
  if (ftell(fo)-fpos != stringdata_used) { fprintf(stderr, "FATAL: INTERNAL ERROR 100\n"); abort(); }
  // tag lists
  printf("writing tag lists...\n");
  for (const mtagvalue_t *tv = tagolist; tv != NULL; tv = tv->hh_o.next) {
    u32 = tv->idx;
    if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  }
  for (const mtagvalue_t *tv = tagtlist; tv != NULL; tv = tv->hh_t.next) {
    u32 = tv->idx;
    if (fwrite(&u32, 4, 1, fo) != 1) goto error;
  }
  // file info
  printf("writing files...\n");
  for (const fileinfo_t *fi = filelist; fi != NULL; fi = fi->hh.next) {
    fl_fileinfo_t tf;
    tf.realname = fi->realname->ofs;
    tf.shortname = fi->shortname->ofs;
#ifdef USE_DEVID_INODE
    tf.devid = fi->di.dev;
    tf.inode = fi->di.node;
#endif
    tf.size = fi->size;
    tf.mtime = fi->mtime;
    tf.tracknum = fi->tracknum;
    tf.year = fi->year;
    tf.year_tag = fi->year_tag->idx;
    tf.artist_o = fi->artist_o->idx;
    tf.album_o = fi->album_o->idx;
    tf.title_o = fi->title_o->idx;
    tf.genre_o = fi->genre_o->idx;
    tf.artist_t = fi->artist_t->idx;
    tf.album_t = fi->album_t->idx;
    tf.title_t = fi->title_t->idx;
    tf.genre_t = fi->genre_t->idx;
    tf.yral_o = fi->yral_o->idx;
    tf.yral_t = fi->yral_t->idx;
    if (fwrite(&tf, sizeof(tf), 1, fo) != 1) goto error;
  }
  // tags
  //fprintf(stderr, "tagbytes_used=%u\n", tagbytes_used);
  printf("writing tags...\n");
  //fpos = ftell(fo);
  for (const mtagvalue_t *tv = taglist; tv != NULL; tv = tv->hh.next) {
    fl_tagvalue_t tt;
    tt.value = tv->value->ofs;
    for (int f = 0; f < TFL_MAX; ++f) {
      tt.tfl[f].count = tv->tfl[f].count;
      tt.tfl[f].idx = tv->tfl[f].idx;
    }
    if (fwrite(&tt, sizeof(tt), 1, fo) != 1) goto error;
  }
  //fprintf(stderr, "(cnt=%d) res=%d\n", cnt, (int)(ftell(fo)-fpos));
  //if (ftell(fo)-fpos != tagbytes_used) { fprintf(stderr, "FATAL: INTERNAL ERROR 101\n"); abort(); }
  // tfls
  printf("writing tfls...\n");
  fpos = ftell(fo);
  for (const mtagvalue_t *tv = taglist; tv != NULL; tv = tv->hh.next) {
    for (int f = 0; f < TFL_MAX; ++f) {
      const tfl_t *tfl = &tv->tfl[f];
      //printf("f=%d; count=%u\n", f, tfl->count);
      for (uint32_t c = 0; c < tfl->count; ++c) {
        //printf(" cnt=%d; fi=%p\n", c, tfl->files[c]);
        u32 = tfl->files[c]->fidx;
        if (fwrite(&u32, 4, 1, fo) != 1) goto error;
      }
    }
  }
  if (ftell(fo)-fpos != tflbytes_used) { fprintf(stderr, "FATAL: INTERNAL ERROR 102\n"); abort(); }
  //
  fclose(fo);
  return 0;
error:
  fprintf(stderr, "FUCK! can't save tag file!\n");
  fclose(fo);
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
// we should cache this, but let FS do it work
static int is_file_in_ignored_dir (const char *filename) {
  static const char *ifiles[] = {
    ".mufignore",
    ".muffinignore",
    ".muf_ignore",
    ".muffin_ignore",
  };
  char *nd = alloca(strlen(filename)+64);
  strcpy(nd, filename);
  for (;;) {
    // check all dirs up to root
    char *nn = strrchr(nd, '/');
    if (nn == NULL || nn == nd) return 0;
    ++nn;
    for (size_t f = 0; f < sizeof(ifiles)/sizeof(ifiles[0]); ++f) {
      strcpy(nn, ifiles[f]);
      if (access(nd, F_OK) == 0) return 1;
    }
    nn[-1] = 0; // strip slash, so next iteration will strip dir name
  }
  return 0;
}


static void ts_build_hashes (void) {
  /*uint64_t stt;*/
  int was_report = 0;
  fileinfo_t *snhash = NULL, *xf; // 'shortname' hash (sanity check)
  string_t *snv;
  int last_dot_count = -1, dot_count;

  mtagvalue_t *add_tag (fileinfo_t *fi, uint32_t tagidx, int tflidx) {
    static const char *defval[TFL_MAX] = {
      UNDEF_YEAR,
      UNDEF_ARTIST_O,
      UNDEF_ARTIST_T,
      UNDEF_ALBUM_O,
      UNDEF_ALBUM_T,
      UNDEF_TITLE_O,
      UNDEF_TITLE_T,
      UNDEF_GENRE_T,
      UNDEF_GENRE_O,
    };
    mtagvalue_t *tv, *tvo;
    int doa;
    //
    switch (tflidx) {
      case TFL_ARTIST_T: tvo = fi->artist_o; break;
      case TFL_ALBUM_T: tvo = fi->album_o; break;
      case TFL_TITLE_T: tvo = fi->title_o; break;
      case TFL_GENRE_T: tvo = fi->genre_o; break;
      default: tvo = NULL; break;
    }
    //
    if (tflidx == TFL_YEAR) doa = 666;
    else if (tflidx == TFL_GENRE_O || tflidx == TFL_GENRE_T) doa = 1;
    else doa = 0;
    //
    tv = tag_add(get_str(get_tv(tagidx)->value), defval[tflidx], (tvo != NULL ? tvo->value : NULL), doa);
    tag_add_fi(tv, fi, tflidx);
    //
    return tv;
  }

  // add fileinfo (and doing stat here)
  printf("checking %u files...\n", file_count);
  /*stt = k8clock();*/
  for (uint32_t f = 0; f < file_count; ++f) {
    fl_fileinfo_t *tfi = get_fi(f);
    const char *fname = get_str(tfi->realname);
    fileinfo_t *fi;
    struct stat st;
    //if (strcmp(get_str(tfi->shortname), ".ogg") == 0) abort();
    if (stat(fname, &st) != 0 || !S_ISREG(st.st_mode)) goto do_progress;/*continue;*/ // either bad or non-existent file
    if (is_file_in_ignored_dir(fname)) goto do_progress; /* this dir is ignored: assume that we have no such file */
    if (/*st.st_ino != tfi->inode || st.st_dev != tfi->devid ||*/ st.st_mtime != tfi->mtime || st.st_size != tfi->size) {
      // file was changed, skip it
      /*
      fprintf(stderr, "[%s]: dev=%u(%u); inode=%u(%u); mtime=%u(%u); size=%u(%u)\n", fname,
        (unsigned int)st.st_dev, (unsigned int)tfi->devid,
        (unsigned int)st.st_ino, (unsigned int)tfi->inode,
        (unsigned int)st.st_mtime, (unsigned int)tfi->mtime,
        (unsigned int)st.st_size, (unsigned int)tfi->size);
      */
      //continue;
      goto do_progress;
    }
    //
    //cstring_add(fname);
    fi = fileinfo_find(fname);
    if (fi != NULL) { fprintf(stderr, "FATAL: internal error 201!\n"); abort(); }
    fi = fileinfo_add(fname);
    //
    snv = cstring_add(get_str(tfi->shortname));
    fi->snamestr = snv->str;
    HASH_FIND_STR_EX(hh_sname, snhash, snv->str, xf);
    if (xf != NULL) { fprintf(stderr, "FATAL: tagdb file corrupted (duplicate shortname found)!\n"); abort(); }
    fi->shortname = snv;
    HASH_ADD_KEYPTR(hh_sname, snhash, fi->snamestr, strlen(fi->snamestr), fi);
    //
#ifdef USE_DEVID_INODE
    fi->di.dev = tfi->devid;
    fi->di.node = tfi->inode;
#endif
    fi->size = tfi->size;
    fi->mtime = tfi->mtime;
    fi->tracknum = tfi->tracknum;
    fi->year = tfi->year;
    //fileinfo_add_inode(fi);
    // add this file tags
    for (int tn = 0; tn < TFL_MAX; ++tn) fi->tags[tn] = add_tag(fi, tfi->tags[tn], tn);
    //
do_progress:
    if (last_dot_count != (dot_count = pbar_dot_count(f+1, file_count))) {
      /*
      uint64_t ctime = k8clock();
      if ((ctime-stt)/200 >= 1) {
        was_report = 1;
        pbar_draw(f+1, file_count);
        stt = ctime;
      }
      */
      last_dot_count = dot_count;
      was_report = 1;
      pbar_draw(f+1, file_count);
    }
  }
  //
  HASH_CLEAR(hh_sname, snhash); // don't need it anymore
  if (was_report) {
    pbar_draw(file_count, file_count);
    pbar_clear();
  }
  tagdb_unload();
  printf("%d files, %d strings, %d tags (%d normal, %d transliterated).\n", HASH_COUNT(filelist), HASH_COUNT(strings), HASH_COUNT(taglist), HASH_CNT(hh_o, tagolist), HASH_CNT(hh_t, tagtlist));
}


////////////////////////////////////////////////////////////////////////////////
static char *getsp (int count, const char *name) {
  static int pos = 0;
  static char bufs[8][256], *res;
  res = bufs[pos];
  pos = (pos+1)%8;
  sprintf(res, "%d %s%s", count, name, (count == 0 || count > 1 ? "s" : ""));
  return res;
}


int main (int argc, char *argv[]) {
  int origfcount, files_lost;
  //
  dbglog_set_screenout(0);
  dbglog_set_fileout(0);
  //
  for (int f = 1; f < argc; ++f) {
    if (strcmp(argv[f], "--except") == 0) {
      struct stat st;
      if (f+1 >= argc) {
        fprintf(stderr, "FATAL: except what?\n");
        return -1;
      }
      if (stat(argv[f+1], &st) == 0 && S_ISDIR(st.st_mode)) {
        direxcept_add(argv[f+1], st.st_dev, st.st_ino);
      }
      for (int c = f+2; c < argc; ++c) argv[c-2] = argv[c];
      argc -= 2;
      --f;
      continue;
    }
    if (strcmp(argv[f], "--verbose") == 0) {
      ++opt_verbose;
      for (int c = f+1; c < argc; ++c) argv[c-1] = argv[c];
      argc -= 1;
      --f;
      continue;
    }
  }
  //
  if (argc < 2) {
    fprintf(stderr, "WTF?!\n");
    return -1;
  }
  //
  if (access(argv[1], R_OK) == 0) {
    if (tagdb_load(argv[1]) != 0) { fprintf(stderr, "FATAL: can't open tagfile!\n"); return -1; }
    origfcount = file_count;
    ts_build_hashes();
    /*
    fileinfo_postprocess();
    tags_postprocess();
    write_tagfile(argv[1]);
    exit(0);
    */
    files_lost = origfcount-HASH_COUNT(filelist);
  } else {
    files_lost = 0;
  }
  //
  if (argc >= 3) {
    int oldfc;
    //taglib_set_strings_unicode(1); // use UTF8
    taglib_set_strings_unicode(0); // use UTF8
    //taglib_id3v2_set_default_text_encoding(TagLib_ID3v2_UTF8); // used only for writing
    oldfc = HASH_COUNT(filelist);
    for (int f = 2; f < argc; ++f) process_dir(argv[f]);
    fileinfo_postprocess();
    tags_postprocess();
    printf("%d files, %d strings, %d tags (%d normal, %d transliterated).\n", HASH_COUNT(filelist), HASH_COUNT(strings), HASH_COUNT(taglist), HASH_CNT(hh_o, tagolist), HASH_CNT(hh_t, tagtlist));
    write_tagfile(argv[1]);
    if (files_lost > 0) printf("%s lost, ", getsp(files_lost, "file"));
    printf("%s processed\n", getsp(HASH_COUNT(filelist)-oldfc, "new file"));
  }
  //
  /*
  for (int f = 0; f < count; ++f) {
    const tagfile_info_t *tfi = &filelist[f];
    if (tfi->artist != NULL && strcasecmp(tfi->artist, "lustre") == 0) {
      printf("lustre: [%s]\n", tfi->filename);
    }
  }
  */
  //
  //flist_clear();
  return 0;
}
