#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>

#include "pbar.h"


static int pbar_tty; // fd or -1


static __attribute__((constructor)) void _ctor_pbar (void) {
  if (isatty(STDOUT_FILENO)) pbar_tty = STDOUT_FILENO;
  else if (isatty(STDERR_FILENO)) pbar_tty = STDERR_FILENO;
  else pbar_tty = -1;
}


static int get_tty_width (void) {
  if (pbar_tty >= 0) {
    struct winsize ws;
    if (ioctl(pbar_tty, TIOCGWINSZ, &ws) != 0 || ws.ws_col < 2) return 79;
    return ws.ws_col-1;
  }
  return 79;
}


/* return number of dots */
static int pbar_draw_ex (uint32_t cur, uint32_t total, int silent) {
  if (pbar_tty >= 0) {
    char cstr[64], tstr[64], wstr[1025];
    int slen, wdt;
    if (cur > total) cur = total;
    snprintf(tstr, sizeof(tstr), "%u", (unsigned int)total);
    slen = strlen(tstr);
    snprintf(cstr, sizeof(cstr), "%*u", slen, (unsigned int)cur);
    slen = (slen*2)+6;
    wdt = get_tty_width();
    if (wdt > 1024) wdt = 1024;
    /* numbers */
    sprintf(wstr, "\r[%s/%s]", cstr, tstr);
    if (!silent) write(pbar_tty, wstr, strlen(wstr));
    if (slen+1 <= wdt) {
      /* dots */
      int dc = (cur >= total ? wdt-slen : ((uint64_t)(wdt-slen))*cur/total), pos;
      strcpy(wstr, " [");
      pos = strlen(wstr);
      if (cur == 0) dc = -1;
      if (silent) return dc+1; /* just return number of dots */
      for (int f = 0; f < wdt-slen; ++f) wstr[pos++] = (f <= dc ? '.' : ' ');
      wstr[pos++] = ']';
      if (!silent) write(pbar_tty, wstr, pos);
    }
  }
  return 0;
}


void pbar_draw (uint32_t cur, uint32_t total) { pbar_draw_ex(cur, total, 0); }
int pbar_dot_count (uint32_t cur, uint32_t total) { return pbar_draw_ex(cur, total, 1); }


void pbar_clear (void) {
  static const char *cstr = "\r\x1b[K";
  if (pbar_tty >= 0) write(pbar_tty, cstr, strlen(cstr));
}
